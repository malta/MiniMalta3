#ifndef MiniMalta3DATA_H
#define MiniMalta3DATA_H

/**********************
 * Class MiniMalta3Data
 * leyre.flores.sanz.de.acedo@cern.ch
 * ignacio.asensi@cern.ch
 * April 2024
 **********************/

#include <vector>
#include <cstdint>
#include <string>

/**
 * A MiniMalta3 word is composed of 64 bits, and can contain up to 16 hits,
 * which are read-out from the FPGA 
 * by the consecutive reading of two 32-bit words with MaltaBase::ReadMaltaWord.
 * These readings are identified as \c word1 and \c word2, and are decoded by MiniMalta3Data.
 * MiniMalta3Data requires an object to be created, and the value of \c word1, and
 * \c word2 to be passed to MiniMalta3Data::setWord1 and MiniMalta3Data::setWord2.
 * The contents of the words then decoded by MiniMalta3Data::unpack, and the meaning
 * of the bits stored in internal memories that are accessible through dedicated
 * methods: MiniMalta3Data::getDummybits, MiniMalta3Data::getChipid, MiniMalta3Data::getDcolumn,
 * MiniMalta3Data::getPixel, MiniMalta3Data::getGroup, MiniMalta3Data::getBcid, MiniMalta3Data::getFtcounter,MiniMalta3Data::getPrioeow
 * Additionally, the row and column values of each hit encoded in a MiniMalta3 word,
 * are available through the MiniMalta3Data::getHitRow and MiniMalta3Data::getHitColumn.
 * The number of avilable hits in the MiniMALTA3 word is MiniMalta3Data::getNhits.
 *
 * It is possible to encode the MiniMALTA3 word as two 32-bit words for testing purposes.
 * This is done by setting the value of the bits with dedicated methods (
 * MiniMalta3Data::setDummybits, MiniMalta3Data::setChipid, MiniMalta3Data::setDcolumn,
 * MiniMalta3Data::setPixel, MiniMalta3Data::setGroup, MiniMalta3Data::setBcid, MiniMalta3Data::setFtcounter,MiniMalta3Data::setPrioeow
 * and then calling MiniMalta3Data::pack that makes the words available through
 * MiniMalta3Data::getWord1 and MiniMalta3Data::getWord2.
 * 
 *
 * @brief Tool encode and decode MALTA2 hit words.
 * @author leyre.flores.sanz.de.acedo@cern.ch
 * @author ignacio.asensi@cern.ch
 * @date April 2024
 **/

class MiniMalta3Data{

 public:
  
  /**
   * @brief Initialize internal arrays
   **/
  MiniMalta3Data();
  
  /**
   * @brief Delete internal arrays
   **/
  ~MiniMalta3Data();

  /**
   * @brief Set pixel hit coordinates (col, row)
   * @param col the x-coordinate of the hit
   * @param row the y-coordinate of the hit
   **/
  void setHit(uint32_t col, uint32_t row);

  /**
   * @brief Set the dummy bits appended by the prio
   * to make a 64 bits word
   * @param value the dummy bits
   **/
  void setDummybits(uint32_t value);

  /**
   * @brief Set chip ID
   * @param value the chip ID
   **/
  void setChipid(uint32_t value);

  /**
   * @brief Set double column 
   * @param value the double column
   **/
  void setDcolumn(uint32_t value);

  /**
   * @brief Set pixel hit pattern (16 bits)
   * @param value the pixel hit pattern
   **/
  void setPixel(uint32_t value);

  /**
   * @brief Set group
   * @param value the group
   **/
  void setGroup(uint32_t value);

  /**
   * @brief Set BCID
   * @param value the BCID to set
   **/
  void setBcid(uint32_t value);

  /**
   * @brief Set FT counter
   * @param value the FT counter to set
   **/
  void setFtcounter(uint32_t value);

  /**
   * @brief Set the prio eow
   * @param value the prio eow
   **/
  void setPrioeow(uint32_t value);

  /**
   * @brief Set word1 as a uint32_t and use only 31 lower bits.
   * @param value the 1st word
   **/
  void setWord1(uint32_t value);

  /**
   * @brief Set word2 as a uint32_t and use only 31 lower bits.
   * @param value the 2nd word
   **/
  void setWord2(uint32_t value);

  /**
   * @brief Get Dummy bits appended by prio
   * @return uint32_t Dummy bits
   **/
  uint32_t getDummybits();

  /**
   * @brief Get chip ID
   * @return uint32_t chip ID
   **/
  uint32_t getChipid();

  /**
   * @brief Get double column
   * @return uint32_t double column
   **/
  uint32_t getDcolumn();

  /**
   * @brief Get pixel group
   * @return uint32_t pixel group
   **/
  uint32_t getPixel();

  /**
   * @brief Get group
   * @return uint32_t group
   **/
  uint32_t getGroup();

  /**
   * @brief Get BCID
   * @return uint32_t Chip BCID
   **/
  uint32_t getBcid();

  /**
   * @brief Get FT counter value
   * @return uint32_t FT counter value
   **/
  uint32_t getFtcounter();

  /**
   * @brief Get Prioeow
   * @return uint32_t prioeow bits
   **/
  uint32_t getPrioeow();

  /**
   * @brief Get word1
   * @return uint32_t word
   **/
  uint32_t getWord1();

  /**
   * @brief Get word2
   * @return uint32_t word
   **/
  uint32_t getWord2();

  /**
   * @brief Get number of hits
   * @return uint32_t number of hits
   **/
  uint32_t getNhits();

  /**
   * @brief Get row for given hit
   * @param hit hit number
   * @return uint32_t row of the hit
   **/
  uint32_t getHitRow(uint32_t hit);

  /**
   * @brief Get column for given hit
   * @param hit hit number
   * @return uint32_t column of the hit
   **/
  uint32_t getHitColumn(uint32_t hit);

  /**
   * @brief Get String representation
   * @return string of bits
   **/
  std::string toString();

  /**
   * @brief Get info
   * @return string 
   **/
  std::string getInfo();
    
  /**
   * @brief Encode the bits into a word
   **/
  void pack();

  /**
   * @brief Decode the word into bits
   **/
  void unPack();

  /**
   * @brief Decode the word into bits
   **/
  void unpack();

  /**
   * @brief Dump the data to the screen
   **/
  void dump();
  
 private:

  uint32_t m_dummybits;
  uint32_t m_chipid;
  uint32_t m_dcolumn;
  uint32_t m_pixel;
  uint32_t m_group;
  uint32_t m_bcid;
  uint32_t m_ftcounter;
  uint32_t m_prioeow;
  uint32_t m_word1;
  uint32_t m_word2;
  uint32_t m_nhits;
  std::vector<uint32_t> m_rows;
  std::vector<uint32_t> m_columns;
};

#endif
