#ifndef MINIMALTA3_H
#define MINIMALTA3_H

#include "ipbus/Uhal.h"
#include <map>
#include <vector>

#include <TH2I.h>
#include <TH1D.h>

class MiniMalta3{
 public:
  static const uint32_t VERSION_CHECK=18;

  static const uint32_t SC_REG0=2; //begining of registers for SC
  static const uint32_t SC_REGN=61;
  static const uint32_t SC_NREGS=76;

  static const uint32_t SIMPLE_WORDS_0=2;
  static const uint32_t SIMPLE_WORDS_N=49;

  static const uint32_t FIFO_WORDS_0=0;
  static const uint32_t FIFO_WORDS_N=1;
  static const uint32_t NTAPS             = 40;   // number of taps


  static const uint32_t IPBADDR_VERSION  	= 0;   // ipbus register, version number
  static const uint32_t IPBADDR_CONTROL     = 1; //!Reset 1b, I2C o SC 1b,
  static const uint32_t IPBADDR_FIFO        = 2; //!< Write slow control FIFO register
  static const uint32_t IPBADDR_SC_RESET    = 4; //!Reset of SC
  //static const uint32_t IPBADDR_VREF_PULSE  = 5;
  //static const uint32_t IPBADDR_SCV_PULSE   = 6;
  static const uint32_t IPBADDR_SLOWDATA    = 5;
  static const uint32_t IPBADDR_SLOWDATA_MON= 6;
  static const uint32_t IPBADDR_FASTCOM_SERIAL = 7; //!< Write slow control FIFO register
  static const uint32_t IPBADDR_FASTCOM_CONTROL  = 8;

  static const uint32_t FIFO1_FULL    = 0x00000001; //!< Busy register, Data FIFO 1 mask for full
  static const uint32_t FIFO2_FULL    = 0x00000002; //!< Busy register, Data FIFO 2 mask for full
  static const uint32_t FIFO1_EMPTY   = 0x00000008; //!< Busy register, Data FIFO 1 mask for empty
  static const uint32_t FIFO2_EMPTY   = 0x00000010; //!< Busy register, Data FIFO 2 mask for empty
  static const uint32_t FIFO1_HALF    = 0x00000040; //!< Busy register, Data FIFO 1 mask for half-full
  static const uint32_t FIFO2_HALF    = 0x00000080; //!< Busy register, Data FIFO 2 mask for half-full
  static const uint32_t L1A_COUNTER   = 0xfff00000; //!< Busy register, L1ID
  static const uint32_t FIFOM_EMPTY   = 0x000020;



  static const uint32_t CTRL_AORESET  = 0x00000004; //!< Control register, rese
  static const uint32_t CTRL_INCTRIG  = 0x00080000; //!< Control register, increase the trigger
  static const uint32_t CTRL_L1A_RST  = 0x00010000; //!< Control register, reset the L1A counter
  static const uint32_t CTRL_L1A_EXT  = 0x00040000; //!< Control register, enable external L1A
  static const uint32_t CTRL_PULSE    = 0x00100000; //!< Control register, pulse
  static const uint32_t CTRL_RO_OFF   = 0x20000000; //!< Control register, disable the read-out
  static const uint32_t CTRL_HALFCOLS = 0x40000000; //!< Control register, disable half columns in the read-out
  static const uint32_t CTRL_HALFROWS = 0x80000000; //!< Control register, disable half rows in the read-out

  static const uint32_t DELAY_FAST_EN = 0x00000040; //!< Delay register, enable fast signal

  enum SLOWCONTROL {

      //All the slowcontrol bits go into an IPBUS FIFO
      MATRIX_maskH_d	          = 0,     //48 bits
      MATRIX_pulseH_d	          = 1,     //48 bits

      CLKDIV_CTRL_syncFT_d          = 2,	    //5 bits
      CLKDIV_CTRL_prio_d            = 3,	    //5 bits
      CLKDIV_CTRL_fastcom_d         = 4,	    //5 bits
      FASTCOM_CTRL_pulseWidth_d     = 5,	    //5 bits
      AURORA_clkComp_d              = 6,      //1 bit
      AURORA_fsp_d	          = 7,      //1 bit
      AURORA_sendSS_d	          = 8,      //1 bit
      AURORA_repeatSS_d	          = 9,      //1 bit
      AURORA_debugEn_d	          = 10,     //1 bit
      AURORA_CTRL_muxO_d	          = 11,     //1 bit
      AURORA_CTRL_muxI_d	          = 12,     //1 bit
      PERIPHERY_maskD_d	          = 13,     //16 bits
      PERIPHERY_maskV_d	          = 14,     //16 bits
      PERIPHERY_maskH_d	          = 15,     //16 bits
      PERIPHERY_pulseV_d	          = 16,     //16 bits
      PERIPHERY_pulseH_d	          = 17,     //16 bits
      PERIPHERY_delayCtrl_d         = 18,     //32 bits
      SYNC_enFT_d	                  = 19,     //4 bits
      SYNC_enBC_d	                  = 20,     //4 bits
      LAPA_enCMFB	                  = 21,     //5 bits
      LAPA_enHBRIDGE	          = 22,     //5 bits
      LAPA_enPRE	                  = 23,     //16 bits
      LAPA_en		          = 24,     //1 bit
      LAPA_setIBCMFB                = 25,     //4 bits
      LAPA_setIVNH	          = 26,     //4 bits
      LAPA_setIVNL	          = 27,     //4 bits
      LAPA_setIVPH	          = 28,     //4 bits
      LAPA_setIVPL	          = 29,     //4 bits
      MONITORING_ctrlSFN            = 30,     //4 bits
      MONITORING_ctrlSFP	          = 31,     //4 bits
      DACS_ctrlITHR                 = 32,     //10 bits
      DACS_ctrlIBIAS	          = 33,     //10 bits
      DACS_ctrlIRESET	          = 34,     //10 bits
      DACS_ctrlICASN	          = 35,     //10 bits
      DACS_ctrlIDB	          = 36,     //10 bits
      DACS_ctrlVL	                  = 37,     //10 bits
      DACS_ctrlVH	                  = 38,     //10 bits
      DACS_ctrlVRESETD	          = 39,     //10 bits
      DACS_ctrlVCLIP	          = 40,     //10 bits
      DACS_ctrlVCAS	          = 41,     //10 bits
      DACS_ctrlVREF	          = 42,     //10 bits
      FASTCOM_CTRL_bypassFC_D       = 43,     //1 bit
      FASTCOM_CTRL_RESET_SC_d       = 44,     //8 bits
      FASTCOM_CTRL_PULSE_SC_d	  = 45,     //8 bits
      PLL_CTRL_src_d		  = 46,     //2 bits
      DEBUG_PRIO_enableSC_d	  = 47,     //2 bits
      DEBUG_SYNC_SC_readmem_d       = 48,     //6 bits
	  CLKDIV_CTRL_syncBCID_d        = 49,      //1 bit

    };

  /**
   * @brief Create an empty MiniMalta3 class. Initialize internal memories.
   */
  MiniMalta3();

  /**
   * @brief Clear the internal memories.
   **/
  ~MiniMalta3();
  
  /**
   * @brief Enable the verbose mode
   * @param enable enable verbose if true
   **/
  void SetVerbose(bool enable);

  /**
   * @brief Set the internal ipbus pointer
   * @param ipb pointer to ipbus::Uhal object
   **/

  void SetIPbus(ipbus::Uhal * ipb);

  /**
   * @brief Get the internal ipbus pointer
   * @return ipbus::Uhal pointer
   **/
  ipbus::Uhal * GetIPbus();
  
  /**
   * @brief Connect to the FPGA through ipbus
   * @param connstr Ipbus connection string: "tcp://hostname:port?target=device:port"
   * @return True if communication was established
   **/
  bool Connect(std::string connstr);

  /**
   * @brief force the defaults into the internal memory
   **/
  void Reset(uint32_t value);

  /**
   * @brief set the default values for the slow control
   * @param force true to write values to chip
   **/
  void SetDefaults(bool force=true);
  
  /**
   * @brief Get the default value of a register
   * @param pos Register to get
   **/
  uint32_t GetDefault(int pos);

  /**
   * @brief Write a register
   * @param pos register to write
   * @param value value to write
   * @param force write immediately to the chip
   **/
  void Write(uint32_t pos, uint32_t value, bool force=true);
  
  /**
   * @brief Write all the registers to the chip from memory
   **/
  void WriteAll();

  /**
   * @brief read
   * @param pos register to read
   * @param force true to read from the chip
   * @return uint32_t word
   **/
  uint32_t Read(int pos, bool force=true);
  
  /**
   * @brief Read all the registers from the chip into memory
   **/
  void ReadAll();


  /**
   * @brief Print the value of the registers to screen
   * @param update force to read from the chip
   **/
  int DumpByWord(bool update=false);
  /**
   * @brief Compare data sent and data receive, count errors
   * @param Force update
   * @return number of errors
   **/
  int CheckSlowControl(bool update=false);

  void PrintFullSCWord();
  int DumpByIPbus(bool update=false);
  
  /**
   * @brief Set the state of the slow control: 0=IDLE, 1=Send to chip
   * @param val New state for the slow cotnrol controller (0=IDLE, 1=Send to chip)
   **/
  void  SetState(int val);

  uint32_t Word(std::string word);
  void ReadMaltaWord(uint32_t * values, uint32_t numwords);
  void SendPulse(std::string word);


  /**
   * @brief Enable/Disable masking on a given pixel row
   * @param row Pixel row
   * @param enable Enable if true, disable if false
   **/
  void SetPixelMaskRow(uint32_t row, bool enable);
  /**
   * @brief Send the available slow control commands to the FPGA
   **/
  void Send();

  /**
   * @brief Enable/Disable pulsing on a given pixel row
   * @param row Pixel row
   * @param enable Enable if true, disable if false
   **/
  void SetPixelPulseRow(uint32_t row, bool enable);

  uint32_t invertBinaryOrder(uint32_t v);
  uint32_t reverse_byte(uint32_t x);

  void CheckConnection();

  void SetDAC(uint32_t pos, uint32_t value, bool enable, bool overide);


  /// @brief Get counter from FIFO. Counts 64 words from deserializer
  /// @return 
  uint32_t GetDebugFIFOSize();


  /// @brief Get debug fifo data
  /// @return 
   std::vector<uint32_t> GetDebugFIFOData();

   void SendFastComSerial(uint32_t cmd, uint32_t data);


 private:

  //! The IPBUS pointer
  ipbus::Uhal * m_ipb;
  std::map<uint32_t, uint64_t> m_values;
  //! Defaults for normal registers
  std::map<uint32_t, uint64_t> defs;
  //std::map<uint32_t, uint32_t> m_values;
  //uint32_t m_values[48];
  uint32_t fifos[2]={48,96};
  uint32_t m_fifoSizes[1];
  uint32_t size_of_fifos;
  //! Non-memory efficient definition of the fifo defaults
  uint32_t defsFIFO[2][48];
  // to be written
  int m_fifos[2][48];//to be written
  int m_fifos_words[2];
  //uint32_t m_values_words[16];
  //std::vector <uint32_t> m_values_words;
  std::map<uint32_t, uint32_t> m_values_r;// read
  int  m_fifos_r[17][512];//read
  int m_values_words_r[11];
  int m_fifos_words_r[136];
  std::map<uint32_t, std::string> names;
  std::map<std::string, uint32_t> addresses;
  std::string values_s[81];
  bool verbose;

  uint32_t m_busy;
  
  void thermomiter_encoder(int pos, uint32_t value);
  void hot_encoder(int pos, uint32_t value);
  uint32_t get_diagonal(int col, int row);

  void initialize_array();

  std::vector<uint32_t> m_mask_row;
  std::vector<uint32_t> m_mask_col;
  std::vector<uint32_t> m_mask_diag;
  std::vector<uint32_t> m_mask_dc;

  std::vector<std::pair<uint32_t,uint32_t>> m_maskedPixelList; //!< Vector of masked pixels

};
#endif

