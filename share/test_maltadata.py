#!/usr/bin/env python
import PyMiniMalta3Data


#data = [4227989322, 1460487391, 4227989322, 1460219127, 4227989322, 1460146031, 4227989322, 1460333979]

#for i in range (0,len(data),2):
#    miniMalta3Data.setWord1(data[i])
#    miniMalta3Data.setWord2(data[i+1])


def reverse_bits(num, num_bits=32):
    binary_str = bin(num)[2:].zfill(num_bits)  # Ensure the binary string has num_bits length
    reversed_binary_str = binary_str[::-1]
    reversed_num = int(reversed_binary_str, 2)
    return reversed_num

def gray_to_decimal(gray_code):
    decimal_value = 0
    for bit in gray_code:
        decimal_value = (decimal_value << 1) | int(bit)
        decimal_value ^= (decimal_value >> 1)
    return decimal_value

def inversegrayCode(n):
    inv = 0;
     
    # Taking xor until
    # n becomes zero
    while(n):
        inv = inv ^ n;
        n = n >> 1;
    return inv;


## Definition of the class
miniMalta3Data = PyMiniMalta3Data.MiniMalta3Data()

## Checking that the methods work
#miniMalta3Data.setDummybits(0x4FF)
#print(miniMalta3Data.getDummybits())

# Full 64 bits from the scope are: fc01ff4a570ad933
word1 = reverse_bits(0xfc01ff4a)
word2 = reverse_bits(0x570c2dbf)
miniMalta3Data.setWord1(word1)
miniMalta3Data.setWord2(word2)
miniMalta3Data.unpack()


print("Word 1 is: {0:032b}".format(miniMalta3Data.getWord1()& 0xFFFFFFFF))
print("Word 2 is: {0:032b}".format(miniMalta3Data.getWord2()& 0xFFFFFFFF))
#print("Word 1 and Word 2 are: {0:032b}  {0:032b}".format(miniMalta3Data.getWord2()& 0xFFFFFFFF , miniMalta3Data.getWord1()& 0xFFFFFFFF))
#print(miniMalta3Data.getWord1() & 0xFFFFFFFF)


#print("Word 1 in binary:")
#print(bin(miniMalta3Data.getWord1() & 0xFFFFFFFF)[2:])


#print("Word 2 is:")
#print(miniMalta3Data.getWord2() & 0xFFFFFFFF) # Print as unsigned integer


#print("Word 2 in binary:")
#print(bin(miniMalta3Data.getWord2() & 0xFFFFFFFF)[2:])


#print("Dummybits are:************************")
print("Dummy bits are: {0:011b}".format(miniMalta3Data.getDummybits()))
#print(bin(miniMalta3Data.getDummybits())[2:])
print("ChipId is: {0:04b}".format(miniMalta3Data.getChipid()))
#print("ChipId is:")
#print(bin(miniMalta3Data.getChipid())[2:])
print("DColumn is: {0:09b}".format(miniMalta3Data.getDcolumn()))
#print("DC column is:")
#print(bin(miniMalta3Data.getDcolumn())[2:])
print("Pixel number is: {0:016b}".format(miniMalta3Data.getPixel()))
#print("Pixel number is:")
#print(bin(miniMalta3Data.getPixel())[2:])
#print("Group is:*************")
print("Group bits are: {0:05b}".format(miniMalta3Data.getGroup()))
#print(bin(miniMalta3Data.getGroup())[2:])
#print("Bcid is:")
print("Bcid is: {0:012b}".format(miniMalta3Data.getBcid()))
Bcid = miniMalta3Data.getBcid()
print("Decimal of gray counter value for BCID is: ")
print(inversegrayCode(Bcid))
#print(bin(miniMalta3Data.getBcid())[2:])
#print("FT counter is:")
print("FT counter is: {0:05b}".format(miniMalta3Data.getFtcounter()))
FtCounter = miniMalta3Data.getFtcounter()
print("Decimal of gray counter value is: ")
print(inversegrayCode(FtCounter))
#print(bin(miniMalta3Data.getFtcounter())[2:])
#print("Prio eow is:")
print("Prio eow is: {0:02b}".format(miniMalta3Data.getPrioeow()))
#print(bin(miniMalta3Data.getPrioeow())[2:])

