#!/usr/bin/env python
# ignacio.asensi@cern.ch
import time
import datetime
import sys
from array import array
import argparse
import os
import signal
#import MiniMaltaMap
import PyMiniMalta3
from ROOT import TCanvas, TFile, TH1F, TH2F, gStyle, TGraph, TTree
import AtlasStyle




cont=True
def signal_handler(sig, frame):
    print('You pressed Ctrl+C!')
    global cont
    cont=False
    pass

signal.signal(signal.SIGINT, signal_handler)


AtlasStyle.SetAtlasStyle()

parser=argparse.ArgumentParser()
parser.add_argument('-n' ,'--n'    , help='number of tests',type=int)
args=parser.parse_args()
numberOfTests=args.n


m=PyMiniMalta3.MiniMalta3()
#m.SetVerbose(True)
st_connect="udp://ep-ade-gw-04:50002" 
m.Connect(st_connect)
print("************************************")
print("You are calling to %s" % st_connect)
m.Connect(st_connect)
print("************************************")
time.sleep(0.1)
#m.SetVerbose(v)


m.Reset(1)
m.SetDefaults(False)
m.WriteAll()# needed because setdefaults is set to False
time.sleep(0.02)
# resets everything on FPGA
m.SetState(0)
time.sleep(0.01)
# start test
m.SetState(1)
time.sleep(0.02)


size=m.GetDebugFIFOSize()
print("Size: ", size)
data=m.GetDebugFIFOData()
print("Data:", data)