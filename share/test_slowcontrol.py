#!/usr/bin/env python
import PyMiniMalta3
import time
import signal
import argparse
import random
import sys
import classmonitor
import Keithley

import PyMiniMalta3Data

from ROOT import TCanvas, TFile, TH1F, TH2F, gStyle, TGraph, TTree
from array import array
from statistics import pstdev

#Test declaration of arguments needed to run it
parser = argparse.ArgumentParser(description='Hi there')
parser.add_argument("-r","--repetitions", help='number of repetitions', type=int, default=1)
parser.add_argument("--gw", help='Gateaway router number:7 or 4"', type=int, default=4)
parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
parser.add_argument("-R","--RESET",help="ONLY reset the chip",action='store_true')
parser.add_argument("--CMD",help="CMD", default="pulse")
parser.add_argument("-V","--VALUE",help="Reset to 1 or 0", type=int, default=1)
parser.add_argument("-m","--monitor",help="monitor DACs",action='store_true')
parser.add_argument("--ntimes", help='loops', type=int, default=1)

parser.add_argument("--dacname", help='', default="NO NAME")
parser.add_argument("--enable", help='', type=int, default=1)
parser.add_argument("--overide", help='', type=int, default=0)
parser.add_argument("--data", help='FASTCOM data', type=int, default=0)
#parser.add_argument("--SET_VPULSE_LOW", help='SET_VPULSE_LOW default is 10', type=int, default=10)





    
args = parser.parse_args()
repetitions=args.repetitions
v=args.verbose
print("Verbose: %s" % str(v))
skip_FIFOs=False
monitorDACs=args.monitor

#SET_VPULSE_LOW=int(args.SET_VPULSE_LOW)
#SET_VPULSE_HIGH=int(args.SET_VPULSE_HIGH)
#SET_VCASN=int(args.SET_VCASN)
#SET_VCLIP=int(args.SET_VCLIP)

m=PyMiniMalta3.MiniMalta3()
st_connect="udp://ep-ade-gw-04:50002"# % args.gw
m.Connect(st_connect)
print("************************************")
print("You are calling to %s" % st_connect)
m.Connect(st_connect)
print("************************************")
time.sleep(0.1)
#m.SetVerbose(v)

#print("m.Word()")
#print(m.Word("SET_ICASN"))



def send_ref_pulse_initial():
    m.SetState(0x2) #Ref Pulse is connected to bit 2 in this IPBUS register
    m.SetState(0x0)
    time.sleep(0.1)

def send_syncmem_pulse():
    m.SetState(0x4) #Ref Pulse is connected to bit 2 in this IPBUS register
    m.SetState(0x0)
    time.sleep(0.1)



    Reset(0) 
    Reset(1) 
    m.SetDefaults(False)

    m.Write(m.Word("FASTCOM_CTRL_RESET_SC_d"),0x00, False)


    m.PrintFullSCWord()
    m.SetState(0)
    m.Send()

    time.sleep(1)

    Reset(0) 
    Reset(1) 
    m.SetDefaults(False)

    m.Write(m.Word("FASTCOM_CTRL_RESET_SC_d"),0xFF, False)
    m.PrintFullSCWord()
    m.SetState(0)
    m.Send()


    time.sleep(1)




def load_default_config():



    Reset(0) 
    Reset(1) 
    m.SetDefaults(True)
    m.PrintFullSCWord()
    m.SetState(0)
    m.Send()
    time.sleep(2)
    m.SetState(1)
    time.sleep(2)
    m.SetState(0)






#########################################################################
# FROM LEYREs DATA SCRIPT



def reverse_bits(num, num_bits=32):
    binary_str = bin(num)[2:].zfill(num_bits)  # Ensure the binary string has num_bits length
    reversed_binary_str = binary_str[::-1]
    reversed_num = int(reversed_binary_str, 2)
    return reversed_num

def FT_counter_to_gray(ft_counter):

    gray_code = 0 
    if ft_counter == int("11111", 2):
        gray_code = 0
    if ft_counter == int("11110", 2):
        gray_code = 1 
    if ft_counter == int("11100", 2):
        gray_code = 2 
    if ft_counter == int("11101", 2):
        gray_code = 3 
    if ft_counter == int("11001", 2):
        gray_code = 4 
    if ft_counter == int("11000", 2):
        gray_code = 5 
    if ft_counter == int("11010", 2):
        gray_code = 6 
    if ft_counter == int("11011", 2):
        gray_code = 7 
    if ft_counter == int("10011", 2):
        gray_code = 8 
    if ft_counter == int("10010", 2):
        gray_code = 9 
    if ft_counter == int("10000", 2):
        gray_code = 10 
    if ft_counter == int("10001", 2):
        gray_code = 11 
    if ft_counter == int("10101", 2):
        gray_code = 12 
    if ft_counter == int("10100", 2):
        gray_code = 13 
    if ft_counter == int("10110", 2):
        gray_code = 14 
    if ft_counter == int("10111", 2):
        gray_code = 15 
    if ft_counter == int("00111", 2):
        gray_code = 16 
    if ft_counter == int("00110", 2):
        gray_code = 17 
    if ft_counter == int("00100", 2):
        gray_code = 18 
    if ft_counter == int("00101", 2):
        gray_code = 19 
    if ft_counter == int("00001", 2):
        gray_code = 20 
    if ft_counter == int("00000", 2):
        gray_code = 21 
    if ft_counter == int("00010", 2):
        gray_code = 22 
    if ft_counter == int("00011", 2):
        gray_code = 23 
    if ft_counter == int("01011", 2):
        gray_code = 24 
    if ft_counter == int("01010", 2):
        gray_code = 25 
    if ft_counter == int("01000", 2):
        gray_code = 26 
    if ft_counter == int("01001", 2):
        gray_code = 27
    if ft_counter == int("01101", 2):
        gray_code = 28 
    if ft_counter == int("01100", 2):
        gray_code = 29 
    if ft_counter == int("01110", 2):
        gray_code = 30 
    if ft_counter == int("01111", 2):
        gray_code = 31 
          
    return gray_code


def inversegrayCode(n):
    inv = 0;
     
    # Taking xor until
    # n becomes zero
    while(n):
        inv = inv ^ n;
        n = n >> 1;
    return inv;


## Definition of the class
miniMalta3Data = PyMiniMalta3Data.MiniMalta3Data()



#########################################################################





def assert_reset_all():


    m.Write(m.Word("FASTCOM_CTRL_bypassFC_D"),0x1, False)
    ##Send reset of all digital blocks with SC (I beleive setting all the bits to one I am resetting them)
    m.Write(m.Word("FASTCOM_CTRL_RESET_SC_d"),0xFF, False)



def unassert_reset_all():


    m.Write(m.Word("FASTCOM_CTRL_bypassFC_D"),0x1, False)
    ##Send reset of all digital blocks with SC (I beleive setting all the bits to one I am resetting them)
    m.Write(m.Word("FASTCOM_CTRL_RESET_SC_d"),0x00, False)




## debug_SC ################


def slow_control_debug_test(): 

    Reset(0) 
    Reset(1) 
    m.SetDefaults(False)

    print("-----------------------------------")
    m.Write(m.Word("PLL_CTRL_src_d"),0x3,False) 
    print("-----------------------------------")
    m.Write(m.Word("DEBUG_PRIO_enableSC_d"),0x3,False)
    m.Write(m.Word("CLKDIV_CTRL_syncFT_d"),0x10,False)
    m.Write(m.Word("CLKDIV_CTRL_prio_d"),0x08, False)
    m.Write(m.Word("SYNC_enFT_d"),0xF, False) 



    #assert_reset_all()
    unassert_reset_all()
    print("-----------------------------------")


    m.SetDAC(m.Word("DACS_ctrlVREF"),0,0, 0)

    m.PrintFullSCWord()
    m.SetState(0)
    m.Send()
    time.sleep(0.5)

    m.SetState(1)
    time.sleep(0.5)

   





def sc_set_fastcom_pulse():


    m.Write(m.Word("FASTCOM_CTRL_bypassFC_D"),0x1, False)
    m.Write(m.Word("FASTCOM_CTRL_RESET_SC_d"),0x00, False)
    m.Write(m.Word("FASTCOM_CTRL_PULSE_SC_d"),0xFF, False)


def sc_reset_fastcom_pulse():



    m.Write(m.Word("FASTCOM_CTRL_bypassFC_D"),0x1, False)
    m.Write(m.Word("FASTCOM_CTRL_RESET_SC_d"),0x00, False)
    m.Write(m.Word("FASTCOM_CTRL_PULSE_SC_d"),0x00, False)







## MATRIX_PULSE ###################

def test_matrix_pulse():



# sends pulse through the IPBus towards the REFPULSE_VALID pad. This triggers the refpulse valid test structure used to activate the testing sync memories, but it is also bypassed and the pulse goes directly to some test sync memories.
 VL_VALUE_GROUP = []
 VL_VALUE_DC_GROUP = []
 VL_VALUE = []



 AVG_DELAY = []
 AVG_STDEV = []

 AVG_DELAY_same_group = []
 AVG_STDEV_same_group = []


 AVG_DELAY_same_DC_group = []
 AVG_STDEV_same_DC_group = []


 pixel_1_ID = 0x8000
 pixel_2_ID = 0x0080
 pixel_3_ID = 0x0100
 pixel_4_ID = 0x0001

 pixel_IDs = [pixel_1_ID, pixel_2_ID, pixel_3_ID, pixel_4_ID]


 GROUP_1_ID = 0x1F
 GROUP_2_ID = 0xF

 GROUP_IDs = [GROUP_1_ID, GROUP_2_ID]

 DC_1_ID = 0x7
 DC_2_ID = 0x6

 DC_IDs = [DC_1_ID, DC_2_ID]












 for VL in range (0,1,1):

# ===========================================
# ================SC CONFIG==================

   Reset(0) 
   Reset(1) 
   m.SetDefaults(False)

   print("-----------------------------------")
   m.Write(m.Word("PLL_CTRL_src_d"),0x3,False) 
   print("-----------------------------------")
   m.Write(m.Word("DEBUG_PRIO_enableSC_d"),0x3,False)
   m.Write(m.Word("CLKDIV_CTRL_syncFT_d"),0x10,False)
   m.Write(m.Word("CLKDIV_CTRL_prio_d"),0x08, False)
   m.Write(m.Word("SYNC_enFT_d"),0xF, False) 





   m.Write(m.Word("PERIPHERY_pulseH_d"),0x0000, False)
   m.Write(m.Word("PERIPHERY_pulseV_d"),0x00F0, False)




   m.Write(m.Word("PERIPHERY_maskH_d"),0x0000, False)
   m.Write(m.Word("PERIPHERY_maskV_d"),0x0000, False)
   m.Write(m.Word("PERIPHERY_maskD_d"),0x0000, False)



   m.SetDAC(m.Word("DACS_ctrlITHR"),20,0, 0)
   m.SetDAC(m.Word("DACS_ctrlIBIAS"),60,0, 0)
   m.SetDAC(m.Word("DACS_ctrlIRESET"),50,0, 0)
   m.SetDAC(m.Word("DACS_ctrlICASN"),30,0, 0)
   m.SetDAC(m.Word("DACS_ctrlIDB"),20,0, 0)
   m.SetDAC(m.Word("DACS_ctrlVL"),VL,0, 0)
   m.SetDAC(m.Word("DACS_ctrlVH"),160,0, 0)
   m.SetDAC(m.Word("DACS_ctrlVRESETD"),150,0, 0)
   m.SetDAC(m.Word("DACS_ctrlVCLIP"),40,0, 0)
   m.SetDAC(m.Word("DACS_ctrlVCAS"),140,0, 0)

   for x in range (0,48):
      m.SetPixelMaskRow(x,0)
      m.SetPixelPulseRow(x,0)


#   m.SetPixelMaskRow(1,1)
#   m.SetPixelPulseRow(1,1)

   m.SetPixelMaskRow(15,1)
   m.SetPixelPulseRow(15,1)


   m.SetPixelMaskRow(16,1)
   m.SetPixelPulseRow(16,1)





#   m.Write(m.Word("PERIPHERY_delayCtrl_d"),0x00070000, False)
#   m.Write(m.Word("PERIPHERY_delayCtrl_d"),0x000B0000, False)
#   m.Write(m.Word("PERIPHERY_delayCtrl_d"),0x000D0000, False)
   m.Write(m.Word("PERIPHERY_delayCtrl_d"),0x000EEE00, False)


#   m.Write(m.Word("PERIPHERY_delayCtrl_d"),0xDDDDDDDD, False)
#   m.Write(m.Word("PERIPHERY_delayCtrl_d"),0xBBBBBBBB, False)
#   m.Write(m.Word("PERIPHERY_delayCtrl_d"),0x77777777, False)
#   m.Write(m.Word("PERIPHERY_delayCtrl_d"),0x70000000, False)









   #assert_reset_all()
   unassert_reset_all()



   sc_set_fastcom_pulse()

   m.PrintFullSCWord()
   m.SetState(0)
   m.Send()
   time.sleep(0.5)

   m.SetState(1)
   time.sleep(0.5)


   accumulated_differences = []
   accumulated_differences_group = []
   accumulated_differences_DC_group = []




##### RUN CONTROL!!!!1


# This loop coontrols the number of pulse bursts
   for y in range (1,12):


# This loop controls the burst length. One must be careful not to overfill the small 256 FIFO, as the script breaks otherwise.
    for x in range (1,18):


	    # REF PULSE
	    # =============
      m.SetState(0)
      m.SetState(0x2) 
      m.SetState(0)
      time.sleep(0.1)




    data=m.GetDebugFIFOData()
    print(type(data))
    time.sleep(0.5)



    ADDR_values = []
    DCID_values = []
    GROUP_values = []
    FT_values = []
    BCID_values = []
    

    FT_differences = []
    FT_differences_overshot = []


    FT_differences_same_DC_group = []
    FT_differences_same_group = []

    for i in range (0,len(data),2):
        if data[i] != 0x00000000:


           word1 = reverse_bits(data[i])
           word2 = reverse_bits(data[i+1])
           miniMalta3Data.setWord1(word1)
           miniMalta3Data.setWord2(word2)
           miniMalta3Data.unpack()

           if ((miniMalta3Data.getDcolumn() != 0x1FF) and (miniMalta3Data.getDummybits() == 0x03F)):

              print("                                   ")
              print("--------------LOOP-----------------")
              print("--------------", i, "-----------------")
              print("-----------------------------------")
              print("Word 1 is: {0:032b}".format(miniMalta3Data.getWord1()& 0xFFFFFFFF))
              print("Word 2 is: {0:032b}".format(miniMalta3Data.getWord2()& 0xFFFFFFFF))

              print("-----------------------------------")
              print("Dummy bits are: {0:011b}".format(miniMalta3Data.getDummybits()))
              print("ChipId is: {0:04b}".format(miniMalta3Data.getChipid()))
              print("DColumn is: {0:09b}".format(miniMalta3Data.getDcolumn()))
              print("Pixel number is: {0:016b}".format(miniMalta3Data.getPixel()))
              print("Group bits are: {0:05b}".format(miniMalta3Data.getGroup()))
              print("                                   ")
              print("Bcid is: {0:012b}".format(miniMalta3Data.getBcid()))
              Bcid = miniMalta3Data.getBcid()
              print("BCID DECIMAL:")
              print("-------------")	
              print(inversegrayCode(Bcid))
              print("-------------")
              print("FT counter is: {0:05b}".format(miniMalta3Data.getFtcounter()))
              FtCounter = miniMalta3Data.getFtcounter()
              print("FT DECIMAL:")
              print("-------------")	
              print(FT_counter_to_gray(FtCounter))
              print(" ")




              ADDR_values.append(miniMalta3Data.getPixel())
              DCID_values.append(miniMalta3Data.getDcolumn())
              GROUP_values.append(miniMalta3Data.getGroup())
              FT_values.append(FT_counter_to_gray(FtCounter))
              BCID_values.append(inversegrayCode(Bcid))




              current_point = len(BCID_values) - 1

              for j in range (0,current_point):

               if (j == current_point):
                continue
               elif (BCID_values[current_point] == BCID_values[j]):



                # =================================================================================================================================================================
                # this if statement filters the hits which were produced by the same group. it will be used for measurements of delay within the asynch address generation circuit.

                if ((DCID_values[j] == DCID_values[current_point]) and (GROUP_values[j] == GROUP_values[current_point])):

                 if ((ADDR_values [current_point] == pixel_1_ID and ADDR_values [j] == pixel_2_ID) or (ADDR_values [current_point] == pixel_2_ID or ADDR_values [j] == pixel_1_ID)):

                  print("DELAYED SAME GROUP HIT DETECTED WITH HIT ID",j)


                  if abs(FT_values[current_point] - FT_values[j]) > 16:
                    continue
                    #if (FT_values[j] < FT_values[current_point]):
                    #  difference = (FT_values[j]+32) - FT_values[current_point]
                    #else:
                    #  difference = (FT_values[current_point]+32) - FT_values[j]

                  else: 
                    difference = abs(FT_values[current_point] - FT_values[j])
                    print("DIFFERENCE IS:---------------------------------------------------------------")
                    print(difference)
                    print("-----------------------------------------------------------------------------")

                    FT_differences_same_DC_group.append(difference)

                  

                 if ((ADDR_values [current_point] == pixel_3_ID and ADDR_values [j] == pixel_4_ID) or (ADDR_values [current_point] == pixel_4_ID or ADDR_values [j] == pixel_3_ID)):

                  print("DELAYED SAME GROUP HIT DETECTED WITH HIT ID",j)


                  if abs(FT_values[current_point] - FT_values[j]) > 16:
                    continue
                    #if (FT_values[j] < FT_values[current_point]):
                    #  difference = (FT_values[j]+32) - FT_values[current_point]
                    #else:
                    #  difference = (FT_values[current_point]+32) - FT_values[j]
 
                  else: 
                    difference = abs(FT_values[current_point] - FT_values[j])
                    print("DIFFERENCE IS:---------------------------------------------------------------")
                    print(difference)
                    print("-----------------------------------------------------------------------------")

                    FT_differences_same_DC_group.append(difference)




                # this if statement filters the hits generated within the same row (indicated by the same group ID).
                # the exact type of comparison is not important, as long as the pixels are within different double columns!
                # =================================================================================================================================================================
                if (GROUP_values[j] == GROUP_values[current_point]):

                 if ((DCID_values[j] == DC_1_ID and DCID_values[current_point] == DC_2_ID) or (DCID_values[j] == DC_2_ID and DCID_values[current_point] == DC_1_ID)):



                  print("SAME ROW HIT with HIT ID",j)


                  if abs(FT_values[current_point] - FT_values[j]) > 16:
                    continue
                    #if (FT_values[j] < FT_values[current_point]):
                    #  difference = (FT_values[j]+32) - FT_values[current_point]
                    #else:
                    #  difference = (FT_values[current_point]+32) - FT_values[j]
 
                  else: 
                    difference = abs(FT_values[current_point] - FT_values[j])
                    print("DIFFERENCE IS:---------------------------------------------------------------")
                    print(difference)
                    print("-----------------------------------------------------------------------------")

                    FT_differences_same_group.append(difference)


                # this will be the generic loop, only looking at the bcid. any combination works, as long as they are close enough!
                # =================================================================================================================================================================




                if abs(FT_values[current_point] - FT_values[j]) > 16:
                 continue
                 #if (FT_values[j] < FT_values[current_point]):
                 #  difference = (FT_values[j]+32) - FT_values[current_point]
                 #else:
                 #  difference = (FT_values[current_point]+32) - FT_values[j]
 
                else: 
                 difference = abs(FT_values[current_point] - FT_values[j])
                 print("DIFFERENCE IS:---------------------------------------------------------------")
                 print(difference)
                 print("-----------------------------------------------------------------------------")

                 FT_differences.append(difference)









# The hits are filtered as there is difficulty when gaining 
    if len(FT_differences_same_DC_group) > 0:
        average_difference_DC_group = sum(FT_differences_same_DC_group) / len(FT_differences_same_DC_group)
        accumulated_differences_DC_group.append(average_difference_DC_group)



        print("-----DC GROUP------")
        print("")
        print("DAC:")
        print(VL)
        print("FT DIFFERENCES SAME DC GROUP!")
        print(FT_differences_same_DC_group)

        print("AVERAGE DIFFERENCE DC GROUP TIME SCALED!")
        print(average_difference_DC_group*0.976)
        print("")
        print("-------------")

    else:
        print("NO VALID DC GROUP HITS THIS RUN")
        average_difference = 0



    if len(FT_differences_same_group) > 0:
        average_difference_group = sum(FT_differences_same_group) / len(FT_differences_same_group)
        accumulated_differences_group.append(average_difference_group)

        print("-----GROUP------")
        print("")
        print("DAC:")
        print(VL)
        print("FT DIFFERENCES SAME_GROUP!")
        print(FT_differences_same_group)

        print("AVERAGE DIFFERENCE GROUP TIME SCALED!")
        print(average_difference_group*0.976)

        print("")
        print("-------------")

    else:
        print("NO VALID GROUP HITS THIS RUN")
        average_difference = 0





    if len(FT_differences) > 0:
        average_difference = sum(FT_differences) / len(FT_differences)
        accumulated_differences.append(average_difference)

        print("-----ALL------")
        print("")
        print("DAC:")
        print(VL)
        print("FT DIFFERENCES GENERAL!")
        print(FT_differences)


        print("AVERAGE DIFFERENCE TIME SCALED!")
        print(average_difference*0.976)
        print("")
        print("-------------")

    else:
        print("NO VALID HITS THIS RUN")
        average_difference = 0






   if len(accumulated_differences_DC_group) > 0:
      total_average_difference_DC_group = sum(accumulated_differences_DC_group) / len(accumulated_differences_DC_group)
      VL_VALUE_DC_GROUP.append(VL)
      AVG_DELAY_same_DC_group.append(round(total_average_difference_DC_group,2))
   else:
      total_average_difference_DC_group = 0


   if len(accumulated_differences_group) > 0:
      total_average_difference_group = sum(accumulated_differences_group) / len(accumulated_differences_group)
      VL_VALUE_GROUP.append(VL)
      AVG_DELAY_same_group.append(round(total_average_difference_group,2))
   else:
      total_average_difference_group = 0



   if len(accumulated_differences) > 0:
      total_average_difference = sum(accumulated_differences) / len(accumulated_differences)
      VL_VALUE.append(VL)
      AVG_DELAY.append(round(total_average_difference,2))
   else:
      total_average_difference = 0





 print("             ")
 print("    END      ")
 print("-------------")
 print("-----!!------")
 print("-------------")
 print("             ")
 print("TESTED VL VALUES")
 print(VL_VALUE_GROUP)
 print("AVERAGE DIFFERENCES SAME GROUP!")
 print(AVG_DELAY_same_group)
 print("             ")
 print("             ")
 print("TESTED VL VALUES")
 print(VL_VALUE_DC_GROUP)
 print("AVERAGE DIFFERENCES SAME DC GROUP!")
 print(AVG_DELAY_same_DC_group)
 print("             ")
 print("             ")
 print("             ")
 print("TESTED VL VALUES")
 print(VL_VALUE_DC_GROUP)
 print("AVERAGE DIFFERENCES ALL!")
 print(AVG_DELAY)
 print("             ")
 print("             ")
 print("             ")




#    m.SetState(0)
#    m.SendPulse("IPBADDR_SCV_PULSE") 








































## SC ###################

def test_sync_mem_debug():

# sends pulse through the IPBus towards the REFPULSE_VALID pad. This triggers the refpulse valid test structure used to activate the testing sync memories, but it is also bypassed and the pulse goes directly to some test sync memories.
 DAC_VALUE = []
 AVG_DELAY = []
 AVG_STDEV = []

 for VAL in range (110,254):

# ===========================================
# ================SC CONFIG==================

   Reset(0) 
   Reset(1) 
   m.SetDefaults(False)

   print("-----------------------------------")
   m.Write(m.Word("PLL_CTRL_src_d"),0x3,False) 
   print("-----------------------------------")
   m.Write(m.Word("DEBUG_PRIO_enableSC_d"),0x3,False)
   m.Write(m.Word("CLKDIV_CTRL_syncFT_d"),0x10,False)
   m.Write(m.Word("CLKDIV_CTRL_prio_d"),0x08, False)
   m.Write(m.Word("SYNC_enFT_d"),0x2, False) 


   #assert_reset_all()
   unassert_reset_all()
   print("-----------------------------------")


   m.SetDAC(m.Word("DACS_ctrlVREF"),VAL,0, 0)

   m.PrintFullSCWord()
   m.SetState(0)
   m.Send()
   time.sleep(0.5)

   m.SetState(1)
   time.sleep(0.5)


# ===========================================



   accumulated_differences = []
   accumulated_stdev = []
   for y in range (1,50):




    for x in range (1,24):


	    # REF PULSE
	    # =============
      m.SetState(0)
      m.SetState(0x2) 
      m.SetState(0)
      time.sleep(0.01)




    data=m.GetDebugFIFOData()
    print(type(data))
    time.sleep(0.5)



    current_addr = None
    previous_addr = None
    current_FT = None
    previous_FT = None
    current_BCID = None
    previous_BCID = None
    FT_differences = []
    FT_differences_overshot = []


    for i in range (0,len(data),2):
        if data[i] != 0x00000000:


           word1 = reverse_bits(data[i])
           word2 = reverse_bits(data[i+1])
           miniMalta3Data.setWord1(word1)
           miniMalta3Data.setWord2(word2)
           miniMalta3Data.unpack()

#           print("                                   ")
#           print("--------------LOOP-----------------")
#           print("--------------", i, "-----------------")
#           print("-----------------------------------")
#           print("Word 1 is: {0:032b}".format(miniMalta3Data.getWord1()& 0xFFFFFFFF))
#           print("Word 2 is: {0:032b}".format(miniMalta3Data.getWord2()& 0xFFFFFFFF))

#           print("-----------------------------------")
#           print("Dummy bits are: {0:011b}".format(miniMalta3Data.getDummybits()))
#           print("ChipId is: {0:04b}".format(miniMalta3Data.getChipid()))
#           print("DColumn is: {0:09b}".format(miniMalta3Data.getDcolumn()))
           print("Pixel number is: {0:016b}".format(miniMalta3Data.getPixel()))
#           print("Group bits are: {0:05b}".format(miniMalta3Data.getGroup()))
#           print("                                   ")
           print("Bcid is: {0:012b}".format(miniMalta3Data.getBcid()))
           Bcid = miniMalta3Data.getBcid()
           print("BCID DECIMAL:")
           print("-------------")	
           print(inversegrayCode(Bcid))
           print("-------------")
           print("FT counter is: {0:05b}".format(miniMalta3Data.getFtcounter()))
           FtCounter = miniMalta3Data.getFtcounter()
           print("FT DECIMAL:")
           print("-------------")	
           print(FT_counter_to_gray(FtCounter))
           print(" ")


           current_FT = FT_counter_to_gray(FtCounter)
           current_BCID = inversegrayCode(Bcid)
           current_addr = miniMalta3Data.getPixel()



# this obviously neglects the case where the BCID is off by one. but leave it as is for now

           if previous_BCID is not None and previous_BCID == current_BCID:

             if current_addr == 0b0010001000100010:
                if current_FT < previous_FT:
                   difference = ((current_FT + 32) - previous_FT)
                   print("-------------")
                   print("OPTION 11 EXECUTED")
                   print("DIFFERENCE IS:---------------------------------------------------------------")
                   print(difference)
                   print("-----------------------------------------------------------------------------")
                else:
                   difference = current_FT - previous_FT
                   print("-------------")
                   print ("OPTION 12 EXECUTED")
                   print("DIFFERENCE IS:---------------------------------------------------------------")
                   print(difference)
                   print("-----------------------------------------------------------------------------")


             if current_addr == 0b1110101001010010:
                if previous_FT < current_FT:
                   difference = ((previous_FT + 32) - current_FT)
                   print("-------------")
                   print ("OPTION 13 EXECUTED")
                   print("DIFFERENCE IS:---------------------------------------------------------------")
                   print(difference)
                   print("-----------------------------------------------------------------------------")
                else:
                   difference = previous_FT - current_FT
                   print("-------------")
                   print ("OPTION 14 EXECUTED")
                   print("DIFFERENCE IS:---------------------------------------------------------------")
                   print(difference)
                   print("-----------------------------------------------------------------------------")  


             FT_differences.append(difference)



           elif previous_BCID is not None and (previous_BCID+1) == current_BCID:

             if current_addr == 0b0010001000100010:
                if current_FT < previous_FT:
                   difference = ((current_FT + 32) - previous_FT) +32
                   print("-------------")
                   print("OPTION 21 EXECUTED")
                   print("DIFFERENCE IS:---------------------------------------------------------------")
                   print(difference)
                   print("-----------------------------------------------------------------------------")
                else:
                   difference = (current_FT - previous_FT) + 32
                   print("-------------")
                   print ("OPTION 22 EXECUTED")
                   print("DIFFERENCE IS:---------------------------------------------------------------")
                   print(difference)
                   print("-----------------------------------------------------------------------------")


             if current_addr == 0b1110101001010010:
                print ("INVALID HIT!")

             FT_differences.append(difference)


           elif previous_BCID is not None and (previous_BCID+2) == current_BCID:

             if current_addr == 0b0010001000100010:
                if current_FT < previous_FT:
                   difference = ((current_FT + 32) - previous_FT) +2*32
                   print("-------------")
                   print("OPTION 31 EXECUTED")
                   print("DIFFERENCE IS:---------------------------------------------------------------")
                   print(difference)
                   print("-----------------------------------------------------------------------------")
                else:
                   difference = (current_FT - previous_FT) + 2*32
                   print("-------------")
                   print ("OPTION 32 EXECUTED")
                   print("DIFFERENCE IS:---------------------------------------------------------------")
                   print(difference)
                   print("-----------------------------------------------------------------------------")


             if current_addr == 0b1110101001010010:
                print ("INVALID HIT!")



             FT_differences.append(difference)





           previous_BCID = current_BCID
           previous_FT = current_FT
           previous_addr = current_addr



# The hits are filtered as there is difficulty when gaining 
    if len(FT_differences) > 0:
        average_difference = sum(FT_differences) / len(FT_differences)



# first filter is removing potential 0 entries from the array in situations where they are impossible to occur.

        if ( min(FT_differences) == 0 and average_difference >= 3 ):

           FT_differences = [k for k in FT_differences if  k != 0]
           average_difference = sum(FT_differences) / len(FT_differences)



# the second filter checks if the array contains more valid inputs, or overshot inputs.

        if (min(FT_differences) <= (average_difference - 16)):

           FT_differences_valid = [k for k in FT_differences if  k <= average_difference ]
           FT_differences_overshot = [k - 32 for k in FT_differences if k > (average_difference)]
        else:
           FT_differences_valid = [k for k in FT_differences if  k <= (average_difference + 15)]
           FT_differences_overshot = [k - 32 for k in FT_differences if k > (average_difference + 16)]

        FT_differences_filt = FT_differences_valid + FT_differences_overshot
        FT_differences_filt = [k for k in FT_differences_filt if  k >= 0]


        print(average_difference)
        print(FT_differences)
        print(FT_differences_valid)
        print(FT_differences_overshot)
        print(FT_differences_filt)
        print("DAC:")
        print(VAL)



    if len(FT_differences) > 0 and len(FT_differences_filt) > 0:
        average_difference = sum(FT_differences_filt) / len(FT_differences_filt)
        accumulated_differences.append(average_difference)
        accumulated_stdev.append(pstdev(FT_differences_filt))



        print("-------------")
        print("-------------")
        print("-----!!------")
        print("-------------")
        print("FT DIFFERENCES!")
        print(FT_differences)

        print("FT DIFFERENCES FILTERED!")
        print(FT_differences_filt)

        print("FT DIFFERENCES FILTERED STDEV!")
        print(accumulated_stdev)



        print("AVERAGE DIFFERENCE!")
        print(average_difference)


        print("AVERAGE DIFFERENCE TIME SCALED!")
        print(average_difference*0.976)

    else:

        print("NO VALID HITS RECORDED THIS RUN")
        average_difference = 0






   if len(accumulated_differences) > 0:
      total_average_difference = sum(accumulated_differences) / len(accumulated_differences)
      total_accumulated_stdev = sum(accumulated_stdev) / len(accumulated_stdev)
      DAC_VALUE.append(VAL)
      AVG_DELAY.append(round(total_average_difference,2))
      AVG_STDEV.append(round(total_accumulated_stdev,2))
   else:
      total_average_difference = 0


 print("             ")
 print("             ")
 print("-------------")
 print("-----!!------")
 print("-------------")
 print("             ")
 print("TESTED DAC VALUES")
 print(DAC_VALUE)
 print("AVERAGE DIFFERENCES PER DAC VALUE!")
 print(AVG_DELAY)
 print("AVERAGE STDEV")
 print(AVG_STDEV)
 print("             ")
 print("-------------")
 print("-----!!------")
 print("-------------")
 print("             ")
 print("             ")



#    m.SetState(0)
#    m.SendPulse("IPBADDR_SCV_PULSE") 












#####################################################################################




def reset_all():



    Reset(0) 
    Reset(1) 
    m.SetDefaults(False)

    #m.Write(m.Word("FASTCOM_CTRL_bypassFC_D"),0x1, False)
    ##Send reset of all digital blocks with SC (I beleive setting all the bits to one I am resetting them)
    m.Write(m.Word("FASTCOM_CTRL_RESET_SC_d"),0xFF, False)


    m.PrintFullSCWord()
    m.SetState(0)
    m.Send()
    time.sleep(2)
    m.SetState(1)
    time.sleep(2)





def test_pulse():





    Reset(0) 
    Reset(1) 
    m.SetDefaults(False)




    ##Changing the priority encoder speed to the same of output serialiser
    m.Write(m.Word("CLKDIV_CTRL_prio_d"),0x01, False)
    ##Change polarity to be compatible with serialiser backup clk
    m.Write(m.Word("CLKDIV_CTRL_syncBCID_d"),0x1, False)
    ##Fastest setting for the fine time counter
#    m.Write(m.Word("CLKDIV_CTRL_syncFT_d"),0x08, False)
    #LF: Enable the PLL and injecting a 160 MHz clock instead
    m.Write(m.Word("PLL_CTRL_src_d"),0x3,False)
    #LF: Enables the debug data path after prio and FIFO          
    m.Write(m.Word("DEBUG_PRIO_enableSC_d"),0x1,False)
    #DD: Sets the voltage reference of the starved current delay element in the experimental refeence pulse generator
 #  m.Write(m.Word("DACS_ctrlVREF"),0x05F, False) 
#    m.Write(m.Word("DACS_ctrlVREF"),0x005, False)
#    m.Write(m.Word("SYNC_enFT_d"),0xF, False) 
#    m.Write(m.Word("SYNC_enBC_d"),0xF, False) 
#    m.Write(m.Word("DEBUG_SYNC_SC_readmem_d"),0x04, False) 

    m.PrintFullSCWord()
    m.SetState(0)
    m.Send()
    time.sleep(2)
    m.SetState(1)
    time.sleep(1) 

    

	

    # sending the REF pulse
    # =====================

    m.SetState(0)
    m.SetState(0x2) #Ref Pulse is connected to bit 2 in this IPBUS register
    m.SetState(0)

#    attempt at creating two subsequent ref pulses - the method is too slow, as the mean distance between the rising edges is around 483 us.
#    m.SetState(0x2) #Ref Pulse is connected to bit 2 in this IPBUS register
#    m.SetState(0)

    time.sleep(0.1)



#     readoing out the SYNC DEBUG
    # ================

    m.SetState(0)
    m.SetState(0x4)
    m.SetState(0)

    time.sleep(1)













def send_sc_word():


    # SEND SC WORD
    # ===============
    #Print the SC word that is going to be sent to the chip
    m.PrintFullSCWord()
    #Resets the slow control variables in order to send the new 436 bits
    m.SetState(0)
    #Send the new SC which has the new values of the specified registers in this test and default for the rest of registers
    m.Send()
    time.sleep(1)
    m.SetState(1) 
    time.sleep(1)


def send_ref_pulse():

    # REF PULSE
    # =============
    m.SetState(0x2) #Ref Pulse is connected to bit 2 in this IPBUS register
    # state 1 starts test
    m.SetState(1)
    m.SetState(0)
    time.sleep(1)

def send_sync_pulse():

    # PULSE SYNC DEBUG
    # ================

    m.SetState(0)
    m.SetState(0x4)
    m.SetState(0)
    time.sleep(3)




def set_sc_sync_debug():

    Reset(0) 
    Reset(1) 
    m.SetDefaults(False)

    #Change VREF dac to get a short reference pulse via the generator (lower values = longer pulse, it should be set to ~700 mV)
    m.Write(m.Word("DACS_ctrlVREF"),0x5F, False) 
    #Disable the counters
    m.Write(m.Word("SYNC_enFT_d"),0x0, False) 
    m.Write(m.Word("SYNC_enBC_d"),0x0, False) 
    ##Change polarity to be compatible with serialiser backup clk
    m.Write(m.Word("CLKDIV_CTRL_syncBCID_d"),0x00, False)
    ##Bypass the Fast Command Encoder
    m.Write(m.Word("FASTCOM_CTRL_bypassFC_D"),0x1, False)
 #   m.Write(m.Word("FASTCOM_CTRL_RESET_SC_d"),0xFF, False)




def reset_malta():




    # STEP_1 - assert reset and send default config


    Reset(0) 
    Reset(1) 
    m.SetDefaults(False)
    assert_reset_all()
    send_sc_word()

    # STEP_2 - unassert reset and still send default config


    time.sleep(5)


    Reset(0) 
    Reset(1) 
    m.SetDefaults(False)
    unassert_reset_all()
    send_sc_word()






def test_prio_debug():

    reset_malta()
    set_sc_sync_debug()
    m.Write(m.Word("DEBUG_PRIO_enableSC_d"),0x3,False)
    time.sleep(1)
    send_sc_word()
    time.sleep(1)

    send_ref_pulse()


def test_sync_mem_debug_reduced():


    # SC SETUP
    # ========
    set_sc_sync_debug()
    #Enable specific bit in the SYNC mem debug
    m.Write(m.Word("DEBUG_SYNC_SC_readmem_d"),0x3F, False) 


    send_sc_word()

    send_ref_pulse()

    send_sync_pulse()




    # SC SETUP
    # ========
    set_sc_sync_debug()
    #Enable specific bit in the SYNC mem debug
    m.Write(m.Word("DEBUG_SYNC_SC_readmem_d"),0x3F, False) 


    send_sc_word()
    send_sync_pulse()


    # SC SETUP
    # ========
    set_sc_sync_debug()
    #Enable specific bit in the SYNC mem debug
    m.Write(m.Word("DEBUG_SYNC_SC_readmem_d"),0x00, False) 


    send_sc_word()
    send_sync_pulse()


    # SC SETUP
    # ========
    set_sc_sync_debug()
    #Enable specific bit in the SYNC mem debug
    m.Write(m.Word("DEBUG_SYNC_SC_readmem_d"),0x00, False) 


    send_sc_word()
    send_sync_pulse()


    # SC SETUP
    # ========
    set_sc_sync_debug()
    #Enable specific bit in the SYNC mem debug
    m.Write(m.Word("DEBUG_SYNC_SC_readmem_d"),0x00, False) 


    send_sc_word()
    send_sync_pulse()

    # SC SETUP
    # ========
    set_sc_sync_debug()
    #Enable specific bit in the SYNC mem debug
    m.Write(m.Word("DEBUG_SYNC_SC_readmem_d"),0x00, False) 


    send_sc_word()
    send_sync_pulse()








def set_sc_for_test():






    # SET SC WORD
    # ===============

    Reset(0) 
    Reset(1) 
    m.SetDefaults(False)

    #Change VREF dac to get a short reference pulse via the generator (lower values = longer pulse, it should be set to ~700 mV)
#    m.Write(m.Word("DACS_ctrlVREF"),0x005, False) 
    #Disable the counters
#    m.Write(m.Word("SYNC_enFT_d"),0xF, False) 
#    m.Write(m.Word("SYNC_enBC_d"),0xF, False) 
    ##Change polarity to be compatible with serialiser backup clk
#    m.Write(m.Word("CLKDIV_CTRL_syncBCID_d"),0x1, False)

    #Enable second bit in the SYNC mem debug
#    m.Write(m.Word("DEBUG_SYNC_SC_readmem_d"),0x3F, False) 



    for x in range (44,48):


       Reset(0) 
       Reset(1) 
       m.SetDefaults(False)

       m.Write(x,0xFFFF, False)

#      needs to be written in order to allow for the debug path to be used!
       m.Write(m.Word("DEBUG_PRIO_enableSC_d"),0x3, False)

       m.PrintFullSCWord()


       print("WORD is: " + str(x))


       m.SetState(0)
       m.Send()
       time.sleep(1)

#----------------
       m.SetState(1)
#----------------
 
       time.sleep(1)

#----------------
       m.SetState(0)
#----------------


#      SEND THE REF PULSE!

       m.SetState(0)
       m.SetState(0x2)

       time.sleep(6)

       if cont==False: break
    


#   MAIN
#   ====================================================
#    m.Write(m.Word("DEBUG_PRIO_enableSC_d"),0x3, False)
#    m.Write(m.Word("PLL_CTRL_src_d"),0x0, False)
#    m.Write(m.Word("DACS_ctrlVREF"),0x200, False) 
#    m.Write(m.Word("SYNC_enFT_d"),0x0, False) 
#    m.Write(m.Word("SYNC_enBC_d"),0xF, False) 
#    m.Write(m.Word("CLKDIV_CTRL_syncBCID_d"),0x00, False)


#   TESTED
#   ====================================================
    #m.Write(m.Word("AURORA_fsp_d"),0x0, False) 
    #m.Write(m.Word("AURORA_sendSS_d"),0x1, False) 
    #m.Write(m.Word("AURORA_repeatSS_d"),0x1, False) 
    #m.Write(m.Word("AURORA_debugEn_d"),0x1, False) 
    #m.Write(m.Word("AURORA_CTRL_muxO_d"),0x1, False) 
    #m.Write(m.Word("AURORA_CTRL_muxI_d"),0x1, False) 
    #m.Write(m.Word("LAPA_en"),0x1, False) 
    #m.Write(m.Word("PLL_CTRL_src_d"),0x3, False) 
    #m.Write(m.Word("DEBUG_PRIO_enableSC_d"),0x3, False)
    #m.Write(m.Word("MONITORING_ctrlSFN"),0xF, False)
    #m.Write(m.Word("MONITORING_ctrlSFP"),0xF, False)
    #m.Write(m.Word("SYNC_enFT_d"),0xF, False) 
    #m.Write(m.Word("SYNC_enBC_d"),0xF, False) 
    #m.Write(m.Word("LAPA_setIBCMFB"),0xF, False) 


#    m.Write(m.Word("DACS_ctrlITHR"),0x000, False) 
#   m.Write(m.Word("DACS_ctrlIBIAS"),0x200, False) 
#    m.Write(m.Word("DACS_ctrlIRESET"),0x200, False) 
#    m.Write(m.Word("DACS_ctrlICASN"),0x200, False) 
#    m.Write(m.Word("DACS_ctrlIDB"),0x200, False) 
#    m.Write(m.Word("DACS_ctrlVL"),0x200, False) 
#    m.Write(m.Word("DACS_ctrlVH"),0x200, False) 
#    m.Write(m.Word("DACS_ctrlVRESETD"),0x200, False) 
#    m.Write(m.Word("DACS_ctrlVCLIP"),0x200, False) 
#    m.Write(m.Word("DACS_ctrlVCAS"),0x200, False)
#    m.Write(m.Word("DACS_ctrlVREF"),0x200, False) 













def sc_defaults_nominal():



    m.Write(m.Word("SYNC_enFT_d"),0xF, False) 
    m.Write(m.Word("SYNC_enBC_d"),0xF, False) 



###################################################
    m.Write(m.Word("PLL_CTRL_src_d"),0x3, False)
###################################################

    m.Write(m.Word("DEBUG_PRIO_enableSC_d"),0x3, False)






#    m.Write(m.Word("DACS_ctrlITHR"),0x005, False) 
#    m.Write(m.Word("DACS_ctrlIBIAS"),0x0FF, False) 
#    m.Write(m.Word("DACS_ctrlIRESET"),0x050, False) 
#    m.Write(m.Word("DACS_ctrlICASN"),0x00F, False) 
#    m.Write(m.Word("DACS_ctrlIDB"),0x040, False) 
#    m.Write(m.Word("DACS_ctrlVL"),0x001, False) 
#    m.Write(m.Word("DACS_ctrlVH"),0x0FF, False) 
#    m.Write(m.Word("DACS_ctrlVRESETD"),0x080, False) 
#    m.Write(m.Word("DACS_ctrlVCLIP"),0x050, False) 
#    m.Write(m.Word("DACS_ctrlVCAS"),0x0B0, False)


    m.Write(m.Word("CLKDIV_CTRL_prio_d"),0x02, False)
    m.Write(m.Word("CLKDIV_CTRL_syncBCID_d"),0x0, False)
    m.Write(m.Word("CLKDIV_CTRL_syncFT_d"),0x02, False)


    m.SetDAC(m.Word("DACS_ctrlITHR"),10,0, 0)
    m.SetDAC(m.Word("DACS_ctrlIBIAS"),180,0, 0)
    m.SetDAC(m.Word("DACS_ctrlIRESET"),50,0, 0)
    m.SetDAC(m.Word("DACS_ctrlICASN"),10,0, 0)
    m.SetDAC(m.Word("DACS_ctrlIDB"),200,0, 0)
    m.SetDAC(m.Word("DACS_ctrlVL"),5,0, 0)
    m.SetDAC(m.Word("DACS_ctrlVH"),150,0, 0)
    m.SetDAC(m.Word("DACS_ctrlVRESETD"),60,0, 0)
    m.SetDAC(m.Word("DACS_ctrlVCLIP"),40,0, 0)
    m.SetDAC(m.Word("DACS_ctrlVCAS"),140,0, 0)

    for x in range (0,48):
        m.SetPixelMaskRow(x,1)




def sc_pulse_address():



    m.Write(m.Word("PERIPHERY_pulseH_d"),0xFFFF, False)
    m.Write(m.Word("PERIPHERY_pulseV_d"),0xFFFF, False)

    m.Write(m.Word("PERIPHERY_delayCtrl_d"),0xEEEEEEEE, False)
#    m.Write(m.Word("PERIPHERY_delayCtrl_d"),0xDDDDDDDD, False)
#    m.Write(m.Word("PERIPHERY_delayCtrl_d"),0xBBBBBBBB, False)
#    m.Write(m.Word("PERIPHERY_delayCtrl_d"),0x77777777, False)



    m.Write(m.Word("PERIPHERY_maskH_d"),0xFFFF, False)
    m.Write(m.Word("PERIPHERY_maskV_d"),0xFFFF, False)
    m.Write(m.Word("PERIPHERY_maskD_d"),0xFFFF, False)





def set_sc():


    Reset(0) 
    Reset(1) 
    m.SetDefaults(False)

    sc_defaults_nominal()
#    sc_pulse_address()
    sc_reset_fastcom_pulse()

#    m.Write(m.Word("DACS_ctrlVREF"),0x200, False) 

    m.PrintFullSCWord()
    m.SetState(0)
    m.Send()
    time.sleep(1)

#----------------

    m.SetState(1)
#----------------
 
    time.sleep(1)
    m.SetState(0)
#----------------


#    m.SetPixelPulseRow(20,1)



    for x in range (0,0):

        m.PrintFullSCWord()


        sc_reset_fastcom_pulse()
        m.Write(m.Word("PERIPHERY_maskH_d"),0x0000, False)

 #       m.SetPixelMaskRow(0,1)

        m.SetState(0)
        m.Send()
        time.sleep(1)

#----------------

        m.SetState(1)
#----------------
 
        time.sleep(1)
        m.SetState(0)
#----------------


        m.PrintFullSCWord()


        sc_set_fastcom_pulse()
        m.Write(m.Word("PERIPHERY_maskH_d"),0xFFFF, False)
 #       m.SetPixelMaskRow(0,0)

        m.SetState(0)
        m.Send()
        time.sleep(1)

#----------------

        m.SetState(1)
#----------------
 
        time.sleep(1)
        m.SetState(0)
#----------------


        m.PrintFullSCWord()


        sc_reset_fastcom_pulse()
        m.Write(m.Word("PERIPHERY_maskH_d"),0x0000, False)

 #       m.SetPixelMaskRow(0,1)

        m.SetState(0)
        m.Send()
        time.sleep(1)

#----------------

        m.SetState(1)
#----------------
 
        time.sleep(1)
        m.SetState(0)
#----------------


        m.PrintFullSCWord()


        sc_set_fastcom_pulse()
 #       m.SetPixelMaskRow(0,0)

        m.SetState(0)
        m.Send()
        time.sleep(1)

#----------------

        m.SetState(1)
#----------------
 
        time.sleep(1)
        m.SetState(0)
#----------------



        if cont==False: break
    




















 #   m.SetState(0x8)


#    time.sleep(1)





## IMPORTANT - THIS BROKEN FUNCTION IS THE ONLY WAY TO GET HE PLL OUTPUT!!!!

def test_sync_mem_debug_BACKUP():

























    for x in range (32,33):





       Reset(0) 
       Reset(1) 
       m.SetDefaults(False)
       sc_defaults_pulse()

       m.Write(x,0x0000, False)

#      needs to be written in order to allow for the debug path to be used!
       m.Write(m.Word("DEBUG_PRIO_enableSC_d"),0x3, False)

       m.PrintFullSCWord()


       print("WORD0 is: " + str(x))


       m.SetState(0)
       m.Send()
       time.sleep(1)

#----------------
       m.SetState(1)
#----------------
 
       time.sleep(1)

#----------------
       m.SetState(0)
#----------------


       m.Write(x,0xFFFF, False)

#      needs to be written in order to allow for the debug path to be used!
       m.Write(m.Word("DEBUG_PRIO_enableSC_d"),0x3, False)

       m.PrintFullSCWord()


       print("WORD1 is: " + str(x))


       m.SetState(0)
       m.Send()
       time.sleep(1)

#----------------
       m.SetState(1)
#----------------
 
       time.sleep(1)

#----------------
       m.SetState(0)
#----------------



       m.Write(x,0x0000, False)

#      needs to be written in order to allow for the debug path to be used!
       m.Write(m.Word("DEBUG_PRIO_enableSC_d"),0x3, False)

       m.PrintFullSCWord()


       print("WORD2 is: " + str(x))


       m.SetState(0)
       m.Send()
       time.sleep(1)

#----------------
       m.SetState(1)
#----------------
 
       time.sleep(1)

#----------------
       m.SetState(0)
#----------------































    # SET SC WORD
    # ===============

    Reset(0) 
    Reset(1) 
    m.SetDefaults(False)

    #Change VREF dac to get a short reference pulse via the generator (lower values = longer pulse, it should be set to ~700 mV)
    m.Write(m.Word("DACS_ctrlVREF"),0x5F, False) 
    #Disable the counters
    m.Write(m.Word("SYNC_enFT_d"),0x0, False) 
    m.Write(m.Word("SYNC_enBC_d"),0x0, False) 
    ##Change polarity to be compatible with serialiser backup clk
    m.Write(m.Word("CLKDIV_CTRL_syncBCID_d"),0x00, False)

    #Enable second bit in the SYNC mem debug
    m.Write(m.Word("DEBUG_SYNC_SC_readmem_d"),0x01, False) 



    # SEND SC WORD
    # ===============
    #Print the SC word that is going to be sent to the chip
    m.PrintFullSCWord()
    #Resets the slow control variables in order to send the new 436 bits
    m.SetState(0)
    #Send the new SC which has the new values of the specified registers in this test and default for the rest of registers
    m.Send()
    time.sleep(1)
    m.SetState(0) 
    time.sleep(1)

    print("1st SC WORD SEGMENT")
    print("           ")





    # REF PULSE
    # =============
    m.SetState(0x2) #Ref Pulse is connected to bit 2 in this IPBUS register
    # state 1 starts test
    m.SetState(1)
    time.sleep(1)



    m.SetState(0)
    time.sleep(1)


    print("1st REF PULSE SEGMENT")
    print("           ")


    m.PrintFullSCWord()
    # PULSE SYNC DEBUG
    # ================


    m.SetState(0)
    m.SetState(0x4)
    time.sleep(1)

    m.SetState(1)
    time.sleep(1)

    m.SetState(0)
   
    print("1st SYNC PULSE SEGMENT")
    print("           ")
## ADDRESS 1


    # SWITCH SYNC DEBUG ADDRESS
    # ==========================

    Reset(0) 
    Reset(1) 
    m.SetDefaults(False)

    #Change VREF dac to get a short reference pulse via the generator (lower values = longer pulse, it should be set to ~700 mV)
    m.Write(m.Word("DACS_ctrlVREF"),0x5F, False) 
    #Disable the counters
    m.Write(m.Word("SYNC_enFT_d"),0xF, False) 
    m.Write(m.Word("SYNC_enBC_d"),0xF, False) 
    ##Change polarity to be compatible with serialiser backup clk
    m.Write(m.Word("CLKDIV_CTRL_syncBCID_d"),0x00, False)

    #Enable second bit in the SYNC mem debug
    m.Write(m.Word("DEBUG_SYNC_SC_readmem_d"),0x08, False) 



    # SEND SC WORD
    # ===============
    #Print the SC word that is going to be sent to the chip
    m.PrintFullSCWord()
    #Resets the slow control variables in order to send the new 436 bits
    m.SetState(0)
    #Send the new SC which has the new values of the specified registers in this test and default for the rest of registers
    m.Send()
    time.sleep(1)
    m.SetState(0) 
    time.sleep(1)


    # PULSE SYNC DEBUG
    # ================

    m.SetState(0)
    m.SetState(0x4)
    m.SetState(1)
    m.SetState(0)



   


def test_chorizo():
    Reset(0)
    Reset(1)
    m.SetDefaults(False)
    m.Write(m.Word("DACS_ctrlVREF"),0xEF,False)
    for x in range(0,47):
        #m.SetPixelMaskRow(x, True)
        #m.SetPixelPulseRow(x, True)
        pass
    
    m.PrintFullSCWord()
    m.SetState(0)
    m.Send()
    time.sleep(1)
    # state 1 starts test
    m.SetState(1)
    time.sleep(0.1)
    
    #m.DumpByWord(False)

def test_DAC(dacname, value, enable, overide):
    print("TESTING %s" % dacname)
    Reset(0)
    Reset(1)
    m.SetDefaults(False)
    m.Write(m.Word("DEBUG_SYNC_SC_readmem_d"),0x3F,False)
    m.SetDAC(m.Word(dacname),value,enable, overide)
    m.PrintFullSCWord()
    m.SetState(0)
    m.Send()
    time.sleep(1)
    # state 1 starts test
    m.SetState(1)
    time.sleep(0.1)
    
    #m.DumpByWord(False)   
def test_defaults():
    # SetDefaults with true calls WriteAll method
    m.SetDefaults(True)
    time.sleep(0.01)
    # resets everything on FPGA
    m.SetState(0)
    time.sleep(0.01)
    # state 1 starts test
    m.SetState(1)
    time.sleep(0.1)
    # reads response from emulator
    m.ReadAll()
    time.sleep(0.1)
    # print on screen result. False because I just ReadAll()
    return m.PrintFullSCWord()
def Reset(v):
    m.Reset(v)
def Refpulsevalid(v):
    m.SetState(v)
    
def test_changing_settings():
    #SetDefaults with false! So I will need to WriteAll()
    m.SetDefaults(False)
    for  x in range(17,62):#17-62 is the range of all the small (non-fifo) words
        m.Write(x,1,False)# put them in 1 to test
        pass
    m.WriteAll()# needed because setdefaults is set to False
    time.sleep(0.1)
    # resets everything on FPGA
    m.SetState(0)
    time.sleep(0.01)
    # start test
    m.SetState(1)
    time.sleep(2.2)
    m.SetState(0)
    return m.Dump(True)

def test_changing_settings_FIFO(x):
    global SET_VPULSE_LOW
    global SET_VPULSE_HIGH
    global SET_VCASN
    global SET_VCLIP
    #SetDefaults with false! So I will need to WriteAll()
    m.Reset()
    m.SetDefaults(False)

    

    #for  x in range(17,62):#17-62 is the range of all the small (non-fifo) words
    #    m.Write(x,0,False)# put them in 1 to test
    #    pass
    #101000....00001
    #m.Write(m.Word("DATAFLOW_MERGERTOLEFT"),1,False)
    #m.Write(m.Word("DATAFLOW_MERGERTORIGHT"),0,False)
    #m.Write(m.Word("DATAFLOW_LMERGERTOLVDS"),1,False)
    m.Write(m.Word("LVDS_VBCMFB")      ,0xC,False)
    m.Write(m.Word("DATAFLOW_ENMERGER"),0x0,False)
    m.Write(m.Word("SET_ITHR")         ,120,False)# put them in 1 to test
    m.Write(m.Word("SET_IDB")          ,120,False)# put them in 1 to test
    #
    '''
    m.Write(m.Word("PULSE_MON_L"),1,False)
    m.Write(m.Word("PULSE_MON_R"),1,False)
    m.Write(m.Word("SWCNTL_DACMONV"),1,False)
    m.Write(m.Word("SWCNTL_DACMONI"),1,False)
    #m.Write(m.Word("SET_IRESET_BIT"),1,False)
    time.sleep(0.5)
    # hot encoding
    print("--")
    #
    
    #if SET_VPULSE_LOW<0 or SET_VPULSE_LOW > 127 or SET_VPULSE_HIGH<0 or SET_VPULSE_HIGH > 127 or SET_VCASN<0 or SET_VCASN > 127 or SET_VCLIP<0 or SET_VCLIP > 127:
    #    print("FORBIDEN VALUE!")
    #    return 0
    print("SET_VPULSE_LOW\t%i" % SET_VPULSE_LOW)
    print("SET_VPULSE_HIGH\t%i" % SET_VPULSE_HIGH)
    print("SET_VCLIP\t%i" % SET_VCLIP)
    print("SET_VCASN\t%i" % SET_VCASN)
    
    m.Write(m.Word("SET_VCASN"),SET_VCASN,False)
    m.Write(m.Word("SET_VCLIP"),SET_VCLIP,False)
    m.Write(m.Word("SET_VPULSE_HIGH"),SET_VPULSE_LOW,False)
    m.Write(m.Word("SET_VPULSE_LOW"),SET_VPULSE_LOW,False)
    
    m.Write(m.Word("SET_VRESET_P"),29,False)
    m.Write(m.Word("SET_VRESET_D"),65,False)
    #'''
    
    #m.Write(m.Word("SET_VCASN"),0,False)
    #m.Write(m.Word("SET_VCLIP"),0,False)
    #m.Write(m.Word("SET_VPLSE_HIGH"),0,False)
    #m.Write(m.Word("SET_VPLSE_LOW"),0,False)
    #m.Write(m.Word("SET_VRESET_P"),0,False)
    #m.Write(m.Word("SET_VRESET_D"),0,False)
    
    # temp encoder
    '''
    
    m.Write(m.Word("SET_ICASN"),5,False)# put them in 1 to test
    m.Write(m.Word("SET_IRESET"),30,False)# put them in 1 to test
    m.Write(m.Word("SET_ITHR"),120,False)# put them in 1 to test
    m.Write(m.Word("SET_IBIAS"),43,False)
    m.Write(m.Word("SET_IDB"),50,False)
    
    #m.Write(m.Word("PULSE_HOR"),0,False)
    
    #m.SetPixelPulseRow(400,True)
    #m.SetPixelPulseColumn(1,True)
    
    for f in range(0,256):
        m.SetDoubleColumnMask(f,True)
        pass
    for dc in range(0, 127):
        m.SetDoubleColumnMask(dc,False)
        pass
    m.SetDoubleColumnMask(170,False)
    #m.SetDoubleColumnMask(164,False)
    m.SetDoubleColumnMask(165,False)
    
    #m.SetDoubleColumnMask(z,False)
    
    ##
    
    
    m.Write(m.Word("SET_IRESET"),0,False)# put them in 1 to test
    m.Write(m.Word("SET_ITHR"),0,False)# put them in 1 to test
    m.Write(m.Word("SET_IBIAS"),0,False)
    m.Write(m.Word("SET_IDB"),0,False)
    m.Write(m.Word("PULSE_HOR"),0,False)
    
    m.Write(m.Word("SWCNTL_DACMONV"),1,False)
    m.Write(m.Word("SWCNTL_DACMONI"),1,False)
    m.Write(m.Word("SET_VPULSE_HIGH"),126,False)
    m.Write(m.Word("SET_ITHR"),30,False)# put them in 1 to test
    #m.Write(m.Word("SET_VCASN"),110,False)# put them in 1 to test
    m.Write(m.Word("SET_ICASN"),10,False)# put them in 1 to test	
    #m.Write(m.Word("SET_VRESETP"),80,False)# put them in 1 to test
    #m.Write(m.Word("SET_IBIAS"),43,False)
    '''
    #SetVPULSE_LOW(110,true)
    #m.SetPixelPulseRow(288,True)
    #m.Write(m.Word("PULSE_COL"),10,False)
    m.SetPixelPulseColumn(x,True)
    m.PrintFullSCWord()
    m.WriteAll()# needed because setdefaults is set to False
    time.sleep(0.01)
    # resets everything on FPGA
    m.SetState(0)
    time.sleep(0.01)
    # start test
    m.SetState(1)
    time.sleep(0.01)
    #m.DumpByWord(True)#DO NOT USE TRUE
    #m.SetState(0)
    return 0# m.CheckSlowControl(False)#DO NOT USE TRUE

def test_SendFastComSerial(cmd,data):
    m.SetDefaults(False)
    if cmd=="reset":
        cmd=0b00110011
    elif cmd=="pulse":
        cmd=0b00110011
    else:
        print("invalid command. Exit")
        return
    m.Write(m.Word("FASTCOM_CTRL_pulseWidth_d"),0x1F,False)
    m.Write(m.Word("FASTCOM_CTRL_bypassFC_D"),0x1,False)
    m.SendFastComSerial(cmd, data)
    m.SetState(0)
    m.Send()
    return 0
    
    
'''
if args.RESET==True:
    m.Reset()
else:
    dc=1
    total_errors=0
    for x in range(0,repetitions):
        #time.sleep(0.1)
        total_errors+=test_changing_settings_FIFO(x)
    print("Number of bits that don't match: %i/4321" % total_errors)
    pass

#m.Reset()
print ("Done")
sys.exit()
'''

cont=True
def signal_handler(sig, frame):
    print('You pressed Ctrl+C!')
    global cont
    cont=False
    pass

signal.signal(signal.SIGINT, signal_handler)


if args.CMD=="SC":
    for x in range (0,args.ntimes):
        if cont==False: break
        #test_pulse()
        test_sync_mem_debug()
        #test_sync_mem_debug_BACKUP()
        #test_sync_mem_debug_reduced()
        #test_prio_debug()
        #reset_malta()
        time.sleep(1)
        pass

if args.CMD=="MATRIX_PULSE":
    for x in range (0,args.ntimes):
        if cont==False: break
        test_matrix_pulse()
        time.sleep(1)
        pass



elif args.CMD=="debug_SC":
    slow_control_debug_test()
elif args.CMD=="ref_pulse":
    send_ref_pulse()
elif args.CMD=="sync_pulse":
    send_syncmem_pulse();
elif args.CMD=="reset_all":
    reset_all()
elif args.CMD=="set_sc":
    for x in range (0,args.ntimes):
        if cont==False: break
        set_sc()
        pass
elif args.CMD=="default_config":
    load_default_config()
elif args.CMD=="pulse":
    Refpulsevalid(args.VALUE)
elif args.CMD=="DACtest":
    test_DAC(args.dacname, args.VALUE, args.enable, args.overide)
elif args.CMD=="runDACscan":
    DACscan(args.dacname, args.VALUE, args.enable, args.overide)
elif args.CMD=="fastCom":
    test_SendFastComSerial("reset",args.data)
else:
    print("Script parameter [%s] not found!!" % args.CMD)