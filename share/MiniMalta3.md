# MiniMalta3 {#PackageMiniMalta3}

This is the software for MiniMALTA3. It requires MaltaDAQ.

## Applications

- MiniMalta3AnalogScan: Application to perform an analog scan on MALTA2. Inject a known charge to a range of pixels.
- MiniMalta3ThresholdScan: Application to perform a threshold scan on MALTA2. Inject an increasing value of charge to a range of pixels, fit an s-curve, and extract the threshold and noise from it.
- MiniMalta3NoiseScan: Application to perform a noise scan. Iteratively run random triggers as a function of IDB until the noisy pixels have been identified and/or masked.

## Libraries

- MiniMalta3: Library containing DAQ classes for MiniMALTA3

### MiniMalta3 library

- MiniMalta3: Control MiniMALTA3
- MiniMalta3Data: description of the data of MiniMALTA3
- MiniMalta3Tree: write data to root files
- MiniMalta3Module: class to run MALTA2 as a plane in MaltaMultiDAQ

