#!/usr/bin/env python
# ignacio.asensi@cern.ch
import time
import datetime
import sys
from array import array
import argparse
import os
import signal
#import MiniMaltaMap
import PyMiniMalta3
from ROOT import TCanvas, TFile, TH1F, TH2F, gStyle, TGraph, TTree
import AtlasStyle


cont=True
def signal_handler(sig, frame):
    print('You pressed Ctrl+C!')
    global cont
    cont=False
    pass

signal.signal(signal.SIGINT, signal_handler)


AtlasStyle.SetAtlasStyle()

parser=argparse.ArgumentParser()
parser.add_argument('-r' ,'--reg'    , help='name of the DAC register to change to run the script',type=str,required=True)
args=parser.parse_args()

variable_st=args.reg


path="minimalta3_dac_scans/unknown/"#W8R19/"
os.system("mkdir -p "+path)

m=PyMiniMalta3.MiniMalta3()
#m.SetVerbose(True)
st_connect="udp://ep-ade-gw-04:50002" 
m.Connect(st_connect)
print("************************************")
print("You are calling to %s" % st_connect)
m.Connect(st_connect)
print("************************************")
time.sleep(0.1)
#m.SetVerbose(v)

m.Reset(1)
m.SetDefaults(False)
m.WriteAll()# needed because setdefaults is set to False
time.sleep(0.02)
# resets everything on FPGA
m.SetState(0)
time.sleep(0.01)
# start test
m.SetState(1)
time.sleep(0.02)

##################################
psuport="/dev/serial/by-id/usb-FTDI_USB__-__Serial_Cable_FTV5PSNT-if03-port0"

if "V" in variable_st in variable_st:
    measure="V"
    print("Measuring Volts")
else:
    measure="I"
    print("Measuring current")


psucontrol=0#"MALTA_PSU"
if psucontrol=="MALTA_PSU":
    import MALTA_PSU
    psu=MALTA_PSU.MALTA_PSU()
    psu.addPSU("monitor",psuport,"k",0,0.0,0.0)
    psu.setVoltageLimit("monitor",1.8)
    psu.setVoltage("monitor",0)
    psu.enablePSU("monitor")
    pass
else:
    import Keithley
    k=Keithley.Keithley(psuport)
    #k.setVerbose(True)
    k.enableOutput(False)
    print(k.getModel())
    if measure=="I":
        k.reset()
        k.setSourceCurrent()
        k.setCurrentRange(1)#0.0007)
        k.setSourceCurrent()
        k.setSourceVoltage()
        k.setCurrentLimit(0.25)
        pass
    elif measure=="V":
        k.setSourceCurrent()
        k.setCurrent(0)
        k.setCurrentLimit(0.25)
        pass
    pass
    k.enableOutput(True)

##################################################################################################

now=datetime.datetime.now()
now_st=now.strftime("%Y%m%d__%H_%M_%S")
hfile = TFile( path+'Malta2DACscan_'+variable_st+"_"+now_st+'.root', 'RECREATE', '' )
if measure=="I":
    h_i_Volt = TH2F( 'hVolt', 'DAC vs Voltage', 128, 0, 127, 40, 0, 0.2 )
    pass

start = 0
n = 255
steps=1
#1
reps=1#10
skipped_DACS=[]

a_DAC, a_measured = array( 'f' ), array( 'f' )
ntuple       = TTree("DAC","")
n_measured     = array('f',(0,))     
n_DAC     = array('f',(0,))  

ntuple.Branch("measured",   n_measured,   "measured/F")
ntuple.Branch("DAC",   n_DAC,   "DAC/F")

attempt=0
values_array = [1,3,7,15,31,63,127]
voltage_lecture = []
count=start
for DAC in range(start,n,steps):
    if cont==False:break
    print ("%s [%s]" % (variable_st, str(DAC)))
    attempt=0
    time.sleep(0.01)
    m.SetDAC(m.Word(variable_st), DAC ,1, 0)
    #m.SetPixelPulseRow(47,0)
    #m.SetDefaults(False)
    m.Send()
    time.sleep(0.02)
    # resets everything on FPGA
    m.SetState(0)
    time.sleep(0.01)
    # start test
    m.SetState(1)
    time.sleep(0.2)
    #m.PrintFullSCWord()
    tval=0
    
    goods=0
    if psucontrol=="MALTA_PSU":
        val=psu.getVoltage("monitor") if measure=="V" else psu.getCurrent(0)
        pass
    else:
        for x in range(0,reps):
            time.sleep(0.1)
            val=0
            tmp=-1
            if measure=="I":
                try:
                    #k.setCurrentRange(20e-3)
                    #k.setCurrentHighPrecision() 
                    tmp= k.getCurrent()*1E6
                    pass
                except:
                    continue
                    pass
                if x==0:
                    time.sleep(0.01)
                    try:
                        #k.setCurrentRange(20e-3)
                        time.sleep(0.15)
                        tmp=k.getCurrent()*-1E6
                    except:
                        continue
                        pass
                    pass
                print("Try [%i]\t Measured: %s" %(x , str(tmp)))
                if tmp<50 and tmp > 0.005:
                    tval+=tmp
                    goods+=1

            elif measure=="V":
               	try:
                    tmp = k.getVoltage()
                    voltage_lecture.append(tmp)
                except:
                    continue
                    pass
                if x == 0 : 
                    time.sleep(0.1)
                    try:
                        tmp=k.getVoltage()
                    except:
                        continue
                        pass
                    pass
                print("Try [%i]\t Measured: %s" %(x , str(tmp)))
                if tmp<2 and tmp > 0:
                    tval+=tmp
                    goods+=1
                pass
            pass
        pass
    if goods<reps:
        print("Bad readings from Keithley... DAC skipped")
        skipped_DACS.append(DAC)
        continue
    measured=tval/(goods)
    #print("MEAN \t Measured:%s\t goods:%s" % (str(measured),str(goods)))
    if DAC==n-1: 
        print("Voltage values measured are: " + str(voltage_lecture) )
        pass
    count+=1
    a_DAC.append(DAC)
    a_measured.append(measured)
    n_measured[0]=measured
    n_DAC[0]=DAC
    #h_i_Volt.Fill(DAC, measured)
    ntuple.Fill()
    pass

if psucontrol=="MALTA_PSU":
    psu.disablePSU("monitor")
else:
    k.enableOutput(False)
    pass

try:
	print("Skipped DACs: " + str(len(skipped_DACS)))
	for x in skipped_DACS:
		print(bin(skipped_DACS[x]))
		pass
except:
	print("ERROR")

#print(skipped_DACS)
print("")
g = TGraph( len(a_measured), a_DAC, a_measured )
g.SetMarkerStyle( 8 )
g.SetMarkerSize(1)
if measure=="V":
    g.GetYaxis().SetTitle( "Voltage [V]")
else:
    g.GetYaxis().SetTitle( "Current [#muA]")

g.GetXaxis().SetTitle( "%s [DAC]" % variable_st.replace("SC_","").replace("SER","").replace("_"," "))
g.Write( "graph_%s" % measure )
ntuple.Write()
c1 = TCanvas( 'c1', 'E', 200, 10, 700, 500 )
g.Draw("AP")
c1.Print(path+'scan_'+variable_st+"_"+now_st+'.pdf')
hfile.Write()
hfile.Close()

print ("Done")
input()
sys.exit()

