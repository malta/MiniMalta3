#include "MiniMalta3/MiniMalta3Data.h"
#include <sstream>
#include <iostream>
#include <bitset>

using namespace std;

MiniMalta3Data::MiniMalta3Data(){
  m_dummybits     = 0;    //11 bits
  m_chipid        = 0;	  //4 bits
  m_dcolumn       = 0;    //9 bits
  m_pixel         = 0;    //16 bits
  m_group         = 0;    //5 bits
  m_bcid          = 0;    //12 bits
  m_ftcounter     = 0;    //5 bits
  m_prioeow       = 0;    //2 bits
  m_word1         = 0;    //IPBus first 32 bits
  m_word2         = 0;    //IPBus second 32 bits
  m_rows.resize(64);      //
  m_columns.resize(16);
}

MiniMalta3Data::~MiniMalta3Data(){}

void MiniMalta3Data::setDummybits(uint32_t value){
  m_dummybits = value;
}

void MiniMalta3Data::setChipid(uint32_t value){
  m_chipid = value;
}

void MiniMalta3Data::setDcolumn(uint32_t value){
  m_dcolumn = value;
}

void MiniMalta3Data::setPixel(uint32_t value){
  m_pixel = value;
}

void MiniMalta3Data::setGroup(uint32_t value){
  m_group = value;
}

void MiniMalta3Data::setBcid(uint32_t value){
  m_bcid = value;
}

void MiniMalta3Data::setFtcounter(uint32_t value){
  m_ftcounter = value;
}

void MiniMalta3Data::setPrioeow(uint32_t value){
  m_prioeow = value;
}

void MiniMalta3Data::setWord1(uint32_t value){
  m_word1 = value;
}

void MiniMalta3Data::setWord2(uint32_t value){
  m_word2 = value;
}

uint32_t MiniMalta3Data::getDummybits(){
  return m_dummybits;
}

uint32_t MiniMalta3Data::getChipid(){
  return m_chipid;
}

uint32_t MiniMalta3Data::getDcolumn(){
  return m_dcolumn;
}

uint32_t MiniMalta3Data::getPixel(){
  return m_pixel;
}

uint32_t MiniMalta3Data::getGroup(){
  return m_group;
}

uint32_t MiniMalta3Data::getBcid(){
  return m_bcid;
}

uint32_t MiniMalta3Data::getFtcounter(){
  return m_ftcounter;
}

uint32_t MiniMalta3Data::getPrioeow(){
  return m_prioeow;
}

uint32_t MiniMalta3Data::getNhits(){
  return m_nhits;
}

uint32_t MiniMalta3Data::getWord1(){
  return m_word1;
}

uint32_t MiniMalta3Data::getWord2(){
  return m_word2;
}

uint32_t MiniMalta3Data::getHitRow(uint32_t hit){
  return m_rows[hit];
}

uint32_t MiniMalta3Data::getHitColumn(uint32_t hit){
  return m_columns[hit];
}

void MiniMalta3Data::pack(){

  m_word1=0;
  m_word2=0;
  m_word1 |= (m_dummybits & 0x4FF) << 0;
  m_word1 |= (m_chipid    & 0xF  ) << 11;
  m_word1 |= (m_dcolumn   & 0x1FF) << 15;
  m_word1 |= (m_pixel     & 0xFF ) << 24;
  m_word2 |= (m_pixel     >> 0x7 ) &  0xFF;     // << (39-31);
  m_word2 |= (m_group     & 0x1F ) << (44-31);
  m_word2 |= (m_bcid      & 0xFFF) << (56-31);
  m_word2 |= (m_ftcounter & 0x1F ) << (61-31);
  m_word2 |= (m_prioeow   & 0x3 )  << (63-31);
  
}


void MiniMalta3Data::unPack(){
  unpack();
}


//W1
//11111100000000011111111101001010
//W2
//01010111000010111001010100100111





void MiniMalta3Data::unpack(){
  m_dummybits = (m_word1 >> 0) & 0x7FF;
  m_chipid    = (m_word1 >> 11) & 0xF; 
  m_dcolumn   = (m_word1 >> 15)  & 0x1FF; 
  m_pixel     = (m_word1 >> 24)  & 0xFF;
  m_pixel    |= ((m_word2) & 0xFF) << 8;
  m_group     = (m_word2 >> 8) & 0x1F;
  m_bcid      = (m_word2 >> 13) & 0xFFF;
  m_ftcounter = (m_word2 >> 25) & 0x1F;
  m_prioeow   = (m_word2 >> 30) & 0x3;

  m_nhits = 0;
  for(uint32_t i=0; i<16;i++){
    if(((m_pixel>>i)&0x1)==0){continue;}

    int column = m_dcolumn*2;
    if(i>7){column+=1;}

    int row = m_group*16;
    if     (i==0 || i== 8){row+=0;}
    else if(i==1 || i== 9){row+=1;}
    else if(i==2 || i==10){row+=2;}
    else if(i==3 || i==11){row+=3;}
    else if(i==4 || i==12){row+=4;}
    else if(i==5 || i==13){row+=5;}
    else if(i==6 || i==14){row+=6;}
    else if(i==7 || i==15){row+=7;}
    m_rows[m_nhits]=row;
    m_columns[m_nhits]=column;
    m_nhits++;
  }
}

//LF: To be checked
void MiniMalta3Data::setHit(uint32_t col, uint32_t row){

  row=row&0x1FF;
  col=col&0x1FF;
  m_dcolumn = col>>1;
  m_group = row >> 4; //divide by 16 (32 groups)
  uint32_t rest = row - (m_group<<4); //multiply by 16
}

string MiniMalta3Data::toString(){
  ostringstream os;
  os << "|" << bitset<11>(m_dummybits)
     << "|" << bitset< 4>(m_chipid)
     << "|" << bitset< 9>(m_dcolumn)
     << "|" << bitset< 16>(m_pixel)
     << "|" << bitset< 5>(m_group)
     << "|" << bitset< 12>(m_bcid)
     << "|" << bitset< 5>(m_ftcounter)
     << "|" << bitset< 2>(m_prioeow)
     << "|";
  return os.str();
}

string MiniMalta3Data::getInfo(){
  ostringstream os;
  os << "Dummybits: "   << getDummybits()
     << "  Chipid: "    << getChipid()
     << "  DoubleCol: " << getDcolumn()
     << "  Pixel: "     << getPixel()
     << "  Group: "     << getGroup()
     << "  Bcid: "      << getBcid()
     << "  FTcounter: " << getFtcounter()
     << "  Prioeow: "   << getPrioeow();
  return os.str();
}

void MiniMalta3Data::dump(){
  cout << getInfo() << endl;
}
