/************************************
 * MiniMalta3
 * Brief: Python module for MiniMalta3
 * 
 * Author: Carlos.Solans@cern.ch
 *         Ignacio.Asensi@cern.ch
 *         Leyre.Flores.Sanz.De.Acedo@cern.ch
 ************************************/

#define PY_SSIZE_T_CLEAN //Not sure we need this
#include <Python.h>
#include <stddef.h>
#include "MiniMalta3/MiniMalta3.h"
#include <iostream>
#include <vector>

#if PY_VERSION_HEX < 0x020400F0

#define Py_CLEAR(op)				\
  do {						\
    if (op) {					\
      PyObject *tmp = (PyObject *)(op);		\
      (op) = NULL;				\
      Py_DECREF(tmp);				\
    }						\
  } while (0)

#define Py_VISIT(op)				\
  do {						\
    if (op) {					\
      int vret = visit((PyObject *)(op), arg);	\
      if (vret)					\
	return vret;				\
    }						\
  } while (0)

#endif //PY_VERSION_HEX < 0x020400F0


#if PY_VERSION_HEX < 0x020500F0

typedef int Py_ssize_t;
#define PY_SSIZE_T_MAX INT_MAX
#define PY_SSIZE_T_MIN INT_MIN
typedef inquiry lenfunc;
typedef intargfunc ssizeargfunc;
typedef intobjargproc ssizeobjargproc;

#endif //PY_VERSION_HEX < 0x020500F0

#ifndef PyVarObject_HEAD_INIT
#define PyVarObject_HEAD_INIT(type, size)	\
  PyObject_HEAD_INIT(type) size,
#endif



#if PY_VERSION_HEX >= 0x03000000

#define MOD_ERROR NULL
#define MOD_RETURN(val) val
#define MOD_INIT(name) PyMODINIT_FUNC PyInit_##name(void)
#define MOD_DEF(ob,name,doc,methods)\
  static struct PyModuleDef moduledef = {\
		     PyModuleDef_HEAD_INIT, name, doc,-1, methods\
  };\
  m = PyModule_Create(&moduledef);\

#else

#define MOD_ERROR 
#define MOD_RETURN(val) 
#define MOD_INIT(name) PyMODINIT_FUNC init##name(void)
#define MOD_DEF(ob,name,doc,methods)\
  ob = Py_InitModule3((char *) name, methods, doc);

#endif //PY_VERSION_HEX >= 0x03000000



/************************************
 *
 * Module declaration
 *
 ************************************/

typedef struct {
  PyObject_HEAD
  MiniMalta3 *obj;
  std::vector<uint32_t> vec;
  //std::vector<MiniMalta3Data> proc;
} PyMiniMalta3;

static int _PyMiniMalta3_init(PyMiniMalta3 *self)
{

    self->obj = new MiniMalta3();
    return 0;
}

static void _PyMiniMalta3_dealloc(PyMiniMalta3 *self)
{
  delete self->obj;
  Py_TYPE(self)->tp_free((PyObject*)self);
}


PyObject * _PyMiniMalta3_SetVerbose(PyMiniMalta3 *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O",&py_enable)){
    Py_INCREF(Py_None);
    std::cout << "ERROR IN SetVerbose py module" << std::endl;
    return Py_None;
  }
  bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetVerbose(enable);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_SetIPbus(PyMiniMalta3 *self, PyObject *args)
{
    /*
    uint32_t pos;
    if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    self->obj->SetIPbus(pos);
    */
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_GetIPbus(PyMiniMalta3 *self)
{
  //ipbus::Uhal* ipb = self->obj->GetIPbus();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_Connect(PyMiniMalta3 *self, PyObject *args)
{
    char * str;
    if(PyArg_ParseTuple(args, (char *) "s",&str)){
      self->obj->Connect(std::string(str));
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMiniMalta3_SetDefaults(PyMiniMalta3 *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O",&py_enable)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool val = py_enable? (bool) PyObject_IsTrue(py_enable) : false;

    self->obj->SetDefaults(val);
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMiniMalta3_GetDefault(PyMiniMalta3 *self, PyObject *args)
{
    uint32_t pos;
    if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    PyObject *py_ret;
    uint32_t ret = self->obj->GetDefault(pos);
    py_ret = PyLong_FromLong(ret);
    return py_ret;
}

PyObject * _PyMiniMalta3_Write(PyMiniMalta3 *self, PyObject *args)
{
    uint32_t pos;
    uint32_t val;
    PyObject *py_enable = NULL;
    if (!PyArg_ParseTuple(args, (char *) "IIO",&pos, &val, &py_enable)){
      std::cout << " PyMiniMalta3 Error in number of parameters!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
      Py_INCREF(Py_None);
      return Py_None;
    }
    bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
    self->obj->Write(pos, val, enable);
    Py_INCREF(Py_None);
    return Py_None;
}
/*
PyObject * _PyMiniMalta3_WriteFIFOword(PyMiniMalta3 *self, PyObject *args)
{
    uint32_t pos;
    uint32_t val;

    if (!PyArg_ParseTuple(args, (char *) "II",&pos, &val)){
      std::cout << "  PyMiniMalta3 Error in number of parameters!!!!!!!!!!!!!!!!!!" << std::endl;
      Py_INCREF(Py_None);
      return Py_None;
    }

    self->obj->WriteFIFOword(pos, val);
    Py_INCREF(Py_None);
    return Py_None;
}
*/
PyObject * _PyMiniMalta3_WriteAll(PyMiniMalta3 *self)
{
      self->obj->WriteAll();
      Py_INCREF(Py_None);
      return Py_None;
}

PyObject * _PyMiniMalta3_Reset(PyMiniMalta3 *self, PyObject *args)
{
	uint32_t pos;
	if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
		Py_INCREF(Py_None);
	return Py_None;
	}
      self->obj->Reset(pos);
      Py_INCREF(Py_None);
      return Py_None;
}

PyObject * _PyMiniMalta3_Read(PyMiniMalta3 *self, PyObject *args)
{
    uint32_t pos;
    PyObject *py_enable = NULL;
    if (!PyArg_ParseTuple(args, (char *) "IO",&pos, &py_enable)){
      Py_INCREF(Py_None);
      return Py_None;
    }

    bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
    PyObject *py_ret;
    uint32_t ret = self->obj->Read(pos, enable);
    py_ret = PyLong_FromLong(ret);
    return py_ret;
}

PyObject * _PyMiniMalta3_ReadAll(PyMiniMalta3 *self)
{
    self->obj->ReadAll();
    Py_INCREF(Py_None);
    return Py_None;
}



PyObject * _PyMiniMalta3_DumpByWord(PyMiniMalta3 *self, PyObject *args)
{
  PyObject *py_update = NULL;
  if (!PyArg_ParseTuple(args, (char *) "|O",&py_update)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool update = py_update? (bool) PyObject_IsTrue(py_update) : false;
  PyObject *py_ret;
  uint32_t ret = self->obj->DumpByWord(update);
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMalta3_CheckSlowControl(PyMiniMalta3 *self, PyObject *args)
{
  PyObject *py_update = NULL;
  if (!PyArg_ParseTuple(args, (char *) "|O",&py_update)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool update = py_update? (bool) PyObject_IsTrue(py_update) : false;
  PyObject *py_ret;
  uint32_t ret = self->obj->CheckSlowControl(update);
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMalta3_DumpByIPbus(PyMiniMalta3 *self, PyObject *args)
{
  PyObject *py_update = NULL;
  if (!PyArg_ParseTuple(args, (char *) "|O",&py_update)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool update = py_update? (bool) PyObject_IsTrue(py_update) : false;
  PyObject *py_ret;
  uint32_t ret = self->obj->DumpByIPbus(update);
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}


PyObject * _PyMiniMalta3_SetState(PyMiniMalta3 *self, PyObject *args)
{
    uint32_t pos;
    if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
      Py_INCREF(Py_None);
      return Py_None;
    }

    self->obj->SetState(pos);
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMiniMalta3_Word(PyMiniMalta3 *self, PyObject *args)
{
  char * str;
  if(!PyArg_ParseTuple(args, (char *) "s",&str)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  PyObject *py_ret;
  uint32_t ret=self->obj->Word(std::string(str));
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMalta3_SendPulse(PyMiniMalta3 *self, PyObject *args)
{
	char * str;
	  if(!PyArg_ParseTuple(args, (char *) "s",&str)){
	    Py_INCREF(Py_None);
	    return Py_None;
	  }

	  self->obj->SendPulse(std::string(str));
	  Py_INCREF(Py_None);
	  return Py_None;
}



PyObject * _PyMiniMalta3_Send(PyMiniMalta3 *self)
{   
  self->obj->Send();
  Py_INCREF(Py_None);
  return Py_None;
}


PyObject * _PyMiniMalta3_PrintFullSCWord(PyMiniMalta3 *self)
{
  self->obj->PrintFullSCWord();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_SetPixelMaskRow(PyMiniMalta3 *self, PyObject *args)
{
  uint32_t row;
  PyObject *py_enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "IO",&row,&py_enable)){
    self->obj->SetPixelMaskRow(row,(py_enable? (bool) PyObject_IsTrue(py_enable) : false));
  }
  Py_INCREF(Py_None);
  return Py_None;
}
PyObject * _PyMiniMalta3_SetPixelPulseRow(PyMiniMalta3 *self, PyObject *args)
{
  uint32_t row;
  PyObject *py_enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "IO",&row,&py_enable)){
    self->obj->SetPixelPulseRow(row,(py_enable? (bool) PyObject_IsTrue(py_enable) : false));
  }
  Py_INCREF(Py_None);
  return Py_None;
}


PyObject * _PyMiniMalta3_SetDAC(PyMiniMalta3 *self, PyObject *args)
{
    uint32_t pos;
    uint32_t val;
    PyObject *py_enable = NULL;
    PyObject *py_overide = NULL;
    if (!PyArg_ParseTuple(args, (char *) "IIOO",&pos, &val, &py_enable, &py_overide)){
      std::cout << " PyMiniMalta3 Error in number of parameters!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
      Py_INCREF(Py_None);
      return Py_None;
    }
    bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
    bool overide = py_overide? (bool) PyObject_IsTrue(py_overide) : false;
    self->obj->SetDAC(pos, val, enable, overide);
    Py_INCREF(Py_None);
    return Py_None;
}


PyObject * _PyMiniMalta3_GetDebugFIFOSize(PyMiniMalta3 *self)
{ 
  PyObject *py_ret;
  uint32_t ret=self->obj->GetDebugFIFOSize();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}


PyObject * _PyMiniMalta3_GetDebugFIFOData(PyMiniMalta3 *self)
{ 
  std::vector<uint32_t> ret = self->obj->GetDebugFIFOData();
  PyObject * py_ret = PyList_New(ret.size());
  for(uint32_t i=0;i<ret.size();i++){
    PyObject * it = PyLong_FromLong(ret.at(i));
    if(PyList_SetItem(py_ret,i,it)!=0){
      Py_INCREF(Py_None);
      return(Py_None);
    }
  }
  return py_ret;
}

PyObject * _PyMiniMalta3_SendFastComSerial(PyMiniMalta3 *self, PyObject *args)
{
    uint32_t cmd;
    uint32_t data;
    if (!PyArg_ParseTuple(args, (char *) "II",&cmd, &data)){
      std::cout << " PyMiniMalta3 Error in _PyMiniMalta3_SendFastComSerial!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
      Py_INCREF(Py_None);
      return Py_None;
    }
    self->obj->SendFastComSerial(cmd, data);
    Py_INCREF(Py_None);
    return Py_None;
}

/*
 *
 *

PyObject * _PyMiniMalta3_SetSCClock(PyMiniMalta3 *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "|O",&py_enable)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetSCClock(enable);
  Py_INCREF(Py_None);
  return Py_None;
}


PyObject * _PyMiniMalta3_SetSCReadOut(PyMiniMalta3 *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "|O",&py_enable)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetSCReadOut(enable);
  Py_INCREF(Py_None);
  return Py_None;
}
 PyObject * _PyMiniMalta3_SetPulseWidth(PyMiniMalta3 *self, PyObject *args)
{
  uint32_t width;
  if(PyArg_ParseTuple(args, (char *) "I",&width)){
    self->obj->SetPulseWidth(width);
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_ReadFifoStatus(PyMiniMalta3 *self)
{
  self->obj->ReadFifoStatus();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_IsFifo1Full(PyMiniMalta3 *self)
{
  PyObject * ret = Py_True;
  if(!self->obj->IsFifo1Full()){ ret = Py_False; }
  Py_INCREF(ret);
  return ret;
}

PyObject * _PyMiniMalta3_IsFifo1Empty(PyMiniMalta3 *self)
{
  PyObject * ret = Py_True;
  if(!self->obj->IsFifo1Empty()){ ret = Py_False; }
  Py_INCREF(ret);
  return ret;
}

PyObject * _PyMiniMalta3_IsFifo1Half(PyMiniMalta3 *self)
{
  PyObject * ret = Py_True;
  if(!self->obj->IsFifo1Half()){ ret = Py_False; }
  Py_INCREF(ret);
  return ret;
}

PyObject * _PyMiniMalta3_IsFifo2Full(PyMiniMalta3 *self)
{
  PyObject * ret = Py_True;
  if(!self->obj->IsFifo2Full()){ ret = Py_False; }
  Py_INCREF(ret);
  return ret;
}

PyObject * _PyMiniMalta3_IsFifo2Empty(PyMiniMalta3 *self)
{
  PyObject * ret = Py_True;
  if(!self->obj->IsFifo2Empty()){ ret = Py_False; }
  Py_INCREF(ret);
  return ret;
}

PyObject * _PyMiniMalta3_IsFifo2Half(PyMiniMalta3 *self)
{
  PyObject * ret = Py_True;
  if(!self->obj->IsFifo2Half()){ ret = Py_False; }
  Py_INCREF(ret);
  return ret;
}

PyObject * _PyMiniMalta3_WriteConstDelays(PyMiniMalta3 *self, PyObject *args)
{
  uint32_t delay1, delay2;
  if (PyArg_ParseTuple(args, (char *) "II",&delay1,&delay2)){
    self->obj->WriteConstDelays(delay1, delay2);
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_WriteTap(PyMiniMalta3 *self, PyObject *args)
{
  uint32_t bit, tap1, tap2;
  if (PyArg_ParseTuple(args, (char *) "III",&bit,&tap1,&tap2)){
    self->obj->WriteTap(bit,tap1,tap2);
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_ReadTap(PyMiniMalta3 *self, PyObject *args)
{
  uint32_t bit;
  if (!PyArg_ParseTuple(args, (char *) "I",&bit)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  std::vector<uint32_t> ret = self->obj->ReadTap(bit);
  PyObject * py_ret = PyList_New(ret.size());
  for(uint32_t i=0;i<ret.size();i++){
    PyObject * it = PyLong_FromLong(ret.at(i));
    if(PyList_SetItem(py_ret,i,it)!=0){
      Py_INCREF(Py_None);
      return(Py_None);
    }
  }
  return py_ret;
}


PyObject * _PyMiniMalta3_WriteTapsToFile(PyMiniMalta3 *self, PyObject *args)
{
  char * str;
  if(PyArg_ParseTuple(args, (char *) "s",&str)){
    self->obj->ReadTapsFromFile(std::string(str));
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_ReadTapsFromFile(PyMiniMalta3 *self, PyObject *args)
{
  char * str;
  if(PyArg_ParseTuple(args, (char *) "s",&str)){
    self->obj->ReadTapsFromFile(std::string(str));
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_Trigger(PyMiniMalta3 *self, PyObject *args)
{
  uint32_t ntimes;
  PyObject *py_withPulse = NULL;
  if(PyArg_ParseTuple(args, (char *) "|IO",&ntimes,&py_withPulse)){
    bool withPulse = py_withPulse? (bool) PyObject_IsTrue(py_withPulse) : false;
    self->obj->Trigger(ntimes,withPulse);
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_SetReadoutDelay(PyMiniMalta3 *self, PyObject *args)
{
  uint32_t delay;
  if (PyArg_ParseTuple(args, (char *) "I",&delay)){
    self->obj->SetReadoutDelay(delay);
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_GetReadoutDelay(PyMiniMalta3 *self)
{
  return PyLong_FromLong(self->obj->GetReadoutDelay());
}

PyObject * _PyMiniMalta3_SetPixelPulse(PyMiniMalta3 *self, PyObject *args)
{
  uint32_t row,col;
  PyObject *py_enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "IIO",&row,&col,&py_enable)){
    self->obj->SetPixelPulse(row,col,(py_enable? (bool) PyObject_IsTrue(py_enable) : false));
  }
  Py_INCREF(Py_None);
  return Py_None;
}



PyObject * _PyMiniMalta3_SetPixelPulseColumn(PyMiniMalta3 *self, PyObject *args)
{
  uint32_t col;
  PyObject *py_enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "IO",&col,&py_enable)){
    self->obj->SetPixelPulseColumn(col,(py_enable? (bool) PyObject_IsTrue(py_enable) : false));
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_SetPixelMask(PyMiniMalta3 *self, PyObject *args)
{
  uint32_t row,col;
  PyObject *py_enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "IIO",&row,&col,&py_enable)){
    self->obj->SetPixelMask(row,col,(py_enable? (bool) PyObject_IsTrue(py_enable) : false));
  }
  Py_INCREF(Py_None);
  return Py_None;
}



PyObject * _PyMiniMalta3_SetPixelMaskColumn(PyMiniMalta3 *self, PyObject *args)
{
  uint32_t col;
  PyObject *py_enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "IO",&col,&py_enable)){
    self->obj->SetPixelMaskColumn(col,(py_enable? (bool) PyObject_IsTrue(py_enable) : false));
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_SetPixelMaskDiag(PyMiniMalta3 *self, PyObject *args)
{
  uint32_t diag;
  PyObject *py_enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "IO",&diag,&py_enable)){
    self->obj->SetPixelMaskDiag(diag,(py_enable? (bool) PyObject_IsTrue(py_enable) : false));
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_SetDoubleColumnMask(PyMiniMalta3 *self, PyObject *args)
{
  uint32_t dc;
  PyObject *py_enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "IO",&dc,&py_enable)){
    self->obj->SetDoubleColumnMask(dc,(py_enable? (bool) PyObject_IsTrue(py_enable) : false));
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_SetDoubleColumnMaskRange(PyMiniMalta3 *self, PyObject *args)
{
  uint32_t dc1,dc2;
  PyObject *py_enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "IIO",&dc1,&dc2,&py_enable)){
    self->obj->SetDoubleColumnMaskRange(dc1,dc2,(py_enable? (bool) PyObject_IsTrue(py_enable) : false));
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_SetROI(PyMiniMalta3 *self, PyObject *args)
{
  uint32_t col1,row1,col2,row2;
  PyObject *py_mask = NULL;
  if(PyArg_ParseTuple(args, (char *) "IIIIO",&col1,&row1,&col2,&row2,&py_mask)){
    self->obj->SetROI(col1,row1,col2,row2,(py_mask? (bool) PyObject_IsTrue(py_mask) : false));
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_SetFullPixelMaskFromFile(PyMiniMalta3 *self, PyObject *args)
{
  char * str;
  if(PyArg_ParseTuple(args, (char *) "s",&str)){
    self->obj->SetFullPixelMaskFromFile(std::string(str));
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_SetExternalL1A(PyMiniMalta3 *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "O",&py_enable)){
    self->obj->SetExternalL1A((py_enable? (bool) PyObject_IsTrue(py_enable) : false));
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_ReadoutEnableExternalL1A(PyMiniMalta3 *self)
{
  self->obj->ReadoutEnableExternalL1A();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_ReadoutDisableExternalL1A(PyMiniMalta3 *self)
{
  self->obj->ReadoutDisableExternalL1A();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_ReadoutOn(PyMiniMalta3 *self)
{
  self->obj->ReadoutOn();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_ReadoutOff(PyMiniMalta3 *self)
{
  self->obj->ReadoutOff();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_EnableFastSignal(PyMiniMalta3 *self)
{
  self->obj->EnableFastSignal();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_DisableFastSignal(PyMiniMalta3 *self)
{
  self->obj->DisableFastSignal();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_SetFastSignal(PyMiniMalta3 *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "O",&py_enable)){
    self->obj->SetFastSignal((py_enable? (bool) PyObject_IsTrue(py_enable) : false));
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_ResetL1Counter(PyMiniMalta3 *self)
{
  self->obj->ResetL1Counter();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_GetL1ID(PyMiniMalta3 *self)
{
  return PyLong_FromLong(self->obj->GetL1ID());
}

PyObject * _PyMiniMalta3_GetFIFO2WordCount(PyMiniMalta3 *self)
{
  return PyLong_FromLong(self->obj->GetFIFO2WordCount());
}

/*
PyObject * _PyMiniMalta3_Set_MASK_FULLCOL(PyMiniMalta3 *self, PyObject *args)
{
  uint32_t dc;
  PyObject *py_enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "IO",&dc,&py_enable)){
    self->obj->Set_MASK_FULLCOL(dc,(py_enable? (bool) PyObject_IsTrue(py_enable) : false));
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3_Set_MASK_FULLCOLRange(PyMiniMalta3 *self, PyObject *args)
{
  uint32_t dc1,dc2;
  PyObject *py_enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "IIO",&dc1,&dc2,&py_enable)){
    self->obj->Set_MASK_FULLCOLRange(dc1,dc2,(py_enable? (bool) PyObject_IsTrue(py_enable) : false));
  }
  Py_INCREF(Py_None);
  return Py_None;
}
*/
/*
------------------------


PyObject * _PyMiniMalta3_getValue(PyMiniMalta3 *self, PyObject *args)
{
    uint32_t pos;
    if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    PyObject *py_ret;
    uint32_t ret = self->obj->getValue(pos);
    py_ret = PyLong_FromLong(ret);
    return py_ret;
}

PyObject * _PyMiniMalta3_MaskAll(PyMiniMalta3 *self, PyObject *args)
{
    PyObject *py_enable = NULL;
    if (!PyArg_ParseTuple(args, (char *) "|O",&py_enable)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
    self->obj->MaskAll(enable);
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMiniMalta3_GetRegisterName(PyMiniMalta3 *self, PyObject *args)
{
    uint32_t pos;
    if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    PyObject *py_ret;
    std::string ret = self->obj->GetRegisterName(pos);
    py_ret = PyUnicode_FromString(ret.c_str());
    return py_ret;
}
*/
////////////////////////////////////////////////////////////////


static PyMethodDef PyMiniMalta3_methods[] = {
  {(char *) "SetVerbose",(PyCFunction) _PyMiniMalta3_SetVerbose, METH_VARARGS, NULL },
  {(char *) "SetIPbus",(PyCFunction) _PyMiniMalta3_SetIPbus, METH_VARARGS, NULL },
  {(char *) "GetIPbus",(PyCFunction) _PyMiniMalta3_GetIPbus, METH_NOARGS, NULL },
  {(char *) "Connect",(PyCFunction) _PyMiniMalta3_Connect, METH_VARARGS, NULL },
  {(char *) "SetDefaults",(PyCFunction) _PyMiniMalta3_SetDefaults, METH_VARARGS, NULL },
  {(char *) "GetDefault",(PyCFunction) _PyMiniMalta3_GetDefault, METH_VARARGS, NULL },
  {(char *) "Write",(PyCFunction) _PyMiniMalta3_Write, METH_VARARGS, NULL },
  {(char *) "WriteAll",(PyCFunction) _PyMiniMalta3_WriteAll, METH_NOARGS, NULL },
  {(char *) "Reset",(PyCFunction) _PyMiniMalta3_Reset, METH_VARARGS, NULL },
  {(char *) "Read",(PyCFunction) _PyMiniMalta3_Read, METH_VARARGS, NULL },
  {(char *) "ReadAll",(PyCFunction) _PyMiniMalta3_ReadAll, METH_NOARGS, NULL },
  //  {(char *) "SetSCReadOut",(PyCFunction)   _PyMiniMalta3_SetSCReadOut, METH_VARARGS, NULL },
//  {(char *) "SetSCClock",(PyCFunction)   _PyMiniMalta3_SetSCClock, METH_VARARGS, NULL },
  {(char *) "SetState",(PyCFunction)   _PyMiniMalta3_SetState, METH_VARARGS, NULL },
  {(char *) "DumpByWord",(PyCFunction) _PyMiniMalta3_DumpByWord, METH_VARARGS, NULL },
  {(char *) "CheckSlowControl",(PyCFunction) _PyMiniMalta3_CheckSlowControl, METH_VARARGS, NULL },
  {(char *) "DumpByIPbus",(PyCFunction) _PyMiniMalta3_DumpByIPbus, METH_VARARGS, NULL },
  {(char *) "Word",(PyCFunction) _PyMiniMalta3_Word, METH_VARARGS, NULL },
  {(char *) "SendPulse",(PyCFunction) _PyMiniMalta3_SendPulse, METH_VARARGS, NULL },
  {(char *) "Send",(PyCFunction) _PyMiniMalta3_Send, METH_NOARGS, NULL },
  {(char *) "PrintFullSCWord",(PyCFunction) _PyMiniMalta3_PrintFullSCWord, METH_NOARGS, NULL },
  {(char *) "SetPixelMaskRow",(PyCFunction) _PyMiniMalta3_SetPixelMaskRow, METH_VARARGS, NULL },
  {(char *) "SetPixelPulseRow",(PyCFunction) _PyMiniMalta3_SetPixelPulseRow, METH_VARARGS, NULL },
  {(char *) "SetDAC",(PyCFunction) _PyMiniMalta3_SetDAC, METH_VARARGS, NULL },
  {(char *) "GetDebugFIFOSize",(PyCFunction) _PyMiniMalta3_GetDebugFIFOSize, METH_VARARGS, NULL },
  {(char *) "GetDebugFIFOData",(PyCFunction) _PyMiniMalta3_GetDebugFIFOData, METH_VARARGS, NULL },
  {(char *) "SendFastComSerial",(PyCFunction) _PyMiniMalta3_SendFastComSerial, METH_VARARGS, NULL },
  
/*
  {(char *) "SetPulseWidth",(PyCFunction) _PyMiniMalta3_SetPulseWidth, METH_VARARGS, NULL },
  {(char *) "ReadFifoStatus",(PyCFunction) _PyMiniMalta3_ReadFifoStatus, METH_NOARGS, NULL },
  {(char *) "IsFifo1Full",(PyCFunction) _PyMiniMalta3_IsFifo1Full, METH_NOARGS, NULL },
  {(char *) "IsFifo1Empty",(PyCFunction) _PyMiniMalta3_IsFifo1Empty, METH_NOARGS, NULL },
  {(char *) "IsFifo1Half",(PyCFunction) _PyMiniMalta3_IsFifo1Half, METH_NOARGS, NULL },
  {(char *) "IsFifo2Full",(PyCFunction) _PyMiniMalta3_IsFifo2Full, METH_NOARGS, NULL },
  {(char *) "IsFifo2Empty",(PyCFunction) _PyMiniMalta3_IsFifo2Empty, METH_NOARGS, NULL },
  {(char *) "IsFifo2Half",(PyCFunction) _PyMiniMalta3_IsFifo2Half, METH_NOARGS, NULL },
  {(char *) "WriteConstDelays",(PyCFunction) _PyMiniMalta3_WriteConstDelays, METH_VARARGS, NULL },
  {(char *) "ReadTap",(PyCFunction) _PyMiniMalta3_ReadTap, METH_VARARGS, NULL },
  {(char *) "WriteTap",(PyCFunction) _PyMiniMalta3_WriteTap, METH_VARARGS, NULL },
  {(char *) "ReadTapsFromFile",(PyCFunction) _PyMiniMalta3_ReadTapsFromFile, METH_VARARGS, NULL },
  {(char *) "WriteTapsToFile",(PyCFunction) _PyMiniMalta3_WriteTapsToFile, METH_VARARGS, NULL },
  {(char *) "Trigger",(PyCFunction) _PyMiniMalta3_Trigger, METH_VARARGS, NULL },
  {(char *) "SetReadoutDelay",(PyCFunction) _PyMiniMalta3_SetReadoutDelay, METH_VARARGS, NULL },
  {(char *) "GetReadoutDelay",(PyCFunction) _PyMiniMalta3_GetReadoutDelay, METH_NOARGS, NULL },
  {(char *) "SetPixelPulse",(PyCFunction) _PyMiniMalta3_SetPixelPulse, METH_VARARGS, NULL },
  {(char *) "SetPixelMask",(PyCFunction) _PyMiniMalta3_SetPixelMask, METH_VARARGS, NULL },

  {(char *) "SetPixelMaskColumn",(PyCFunction) _PyMiniMalta3_SetPixelMaskColumn, METH_VARARGS, NULL },
  {(char *) "SetPixelMaskDiag",(PyCFunction) _PyMiniMalta3_SetPixelMaskDiag, METH_VARARGS, NULL },
  {(char *) "SetPixelPulseColumn",(PyCFunction) _PyMiniMalta3_SetPixelPulseColumn, METH_VARARGS, NULL },
  {(char *) "SetDoubleColumnMask",(PyCFunction) _PyMiniMalta3_SetDoubleColumnMask, METH_VARARGS, NULL },
  {(char *) "SetDoubleColumnMaskRange",(PyCFunction) _PyMiniMalta3_SetDoubleColumnMaskRange, METH_VARARGS, NULL },
  // {(char *) "Set_MASK_FULLCOL",(PyCFunction) _PyMiniMalta3_Set_MASK_FULLCOL, METH_VARARGS, NULL },
  //{(char *) "Set_MASK_FULLCOLRange",(PyCFunction) _PyMiniMalta3_Set_MASK_FULLCOLRange, METH_VARARGS, NULL },
  {(char *) "SetFullPixelMaskFromFile",(PyCFunction) _PyMiniMalta3_SetFullPixelMaskFromFile, METH_VARARGS, NULL },
  {(char *) "SetROI",(PyCFunction) _PyMiniMalta3_SetROI, METH_VARARGS, NULL },
  {(char *) "SetExternalL1A",(PyCFunction) _PyMiniMalta3_SetExternalL1A, METH_VARARGS, NULL },
  {(char *) "ReadoutEnableExternalL1A",(PyCFunction) _PyMiniMalta3_ReadoutEnableExternalL1A, METH_NOARGS, NULL },
  {(char *) "ReadoutDisableExternalL1A",(PyCFunction) _PyMiniMalta3_ReadoutDisableExternalL1A, METH_NOARGS, NULL },
  {(char *) "ReadoutOn",(PyCFunction) _PyMiniMalta3_ReadoutOn, METH_NOARGS, NULL },
  {(char *) "ReadoutOff",(PyCFunction) _PyMiniMalta3_ReadoutOff, METH_NOARGS, NULL },
  {(char *) "EnableFastSignal",(PyCFunction) _PyMiniMalta3_EnableFastSignal, METH_NOARGS, NULL },
  {(char *) "DisableFastSignal",(PyCFunction) _PyMiniMalta3_DisableFastSignal, METH_NOARGS, NULL },
  {(char *) "ResetL1Counter",(PyCFunction) _PyMiniMalta3_ResetL1Counter, METH_NOARGS, NULL },
  {(char *) "GetL1ID",(PyCFunction) _PyMiniMalta3_GetL1ID, METH_NOARGS, NULL },
  {(char *) "GetFIFO2WordCount",(PyCFunction) _PyMiniMalta3_GetFIFO2WordCount, METH_NOARGS, NULL },


  {(char *) "getValue",(PyCFunction) _PyMiniMalta3_getValue, METH_VARARGS, NULL },
  {(char *) "MaskAll",(PyCFunction) _PyMiniMalta3_MaskAll, METH_VARARGS, NULL },
  {(char *) "GetRegisterName",(PyCFunction) _PyMiniMalta3_GetRegisterName, METH_VARARGS, NULL },
  */
  {NULL, NULL, 0, NULL}
};

PyTypeObject PyMiniMalta3_Type = {
    PyVarObject_HEAD_INIT(NULL, 0)
    (char *) "PyMiniMalta3.MiniMalta3",            /* tp_name */
    sizeof(PyMiniMalta3),                 /* tp_basicsize */
    0,                                   /* tp_itemsize */
    /* methods */
    (destructor) _PyMiniMalta3_dealloc,   /* tp_dealloc */
    (printfunc)0,                        /* tp_print */
    (getattrfunc)NULL,                   /* tp_getattr */
    (setattrfunc)NULL,                   /* tp_setattr */
    0,                                   /* tp_compare */
    (reprfunc)NULL,                      /* tp_repr */
    (PyNumberMethods*)NULL,              /* tp_as_number */
    (PySequenceMethods*)NULL,            /* tp_as_sequence */
    (PyMappingMethods*)NULL,             /* tp_as_mapping */
    (hashfunc)NULL,                      /* tp_hash */
    (ternaryfunc)NULL,                   /* tp_call */
    (reprfunc)NULL,                      /* tp_str */
    (getattrofunc)NULL,                  /* tp_getattro */
    (setattrofunc)NULL,                  /* tp_setattro */
    (PyBufferProcs*)NULL,                /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT,                  /* tp_flags */
    NULL,                                /* Documentation string */
    (traverseproc)NULL,                  /* tp_traverse */
    (inquiry)NULL,                       /* tp_clear */
    (richcmpfunc)NULL,                   /* tp_richcompare */
    0,                                   /* tp_weaklistoffset */
    (getiterfunc)NULL,                   /* tp_iter */
    (iternextfunc)NULL,                  /* tp_iternext */
    (struct PyMethodDef*)PyMiniMalta3_methods, /* tp_methods */
    (struct PyMemberDef*)0,              /* tp_members */
    0,                                   /* tp_getset */
    NULL,                                /* tp_base */
    NULL,                                /* tp_dict */
    (descrgetfunc)NULL,                  /* tp_descr_get */
    (descrsetfunc)NULL,                  /* tp_descr_set */
    0,                                   /* tp_dictoffset */
    (initproc)_PyMiniMalta3_init,         /* tp_init */
    (allocfunc)PyType_GenericAlloc,      /* tp_alloc */
    (newfunc)PyType_GenericNew,          /* tp_new */
    (freefunc)0,                         /* tp_free */
    (inquiry)NULL,                       /* tp_is_gc */
    NULL,                                /* tp_bases */
    NULL,                                /* tp_mro */
    NULL,                                /* tp_cache */
    NULL,                                /* tp_subclasses */
    NULL,                                /* tp_weaklist */
    (destructor) NULL                    /* tp_del */
#if PY_VERSION_HEX >= 0x02060000
    ,0                               /* tp_version_tag */
#endif
};

static PyMethodDef module_methods[] = {
  {NULL, NULL, 0, NULL}
};

MOD_INIT(PyMiniMalta3)
{
  PyObject *m;
  MOD_DEF(m, "PyMiniMalta3", NULL, module_methods);
  if(PyType_Ready(&PyMiniMalta3_Type)<0){
    return MOD_ERROR;
  }
  PyModule_AddObject(m, (char *) "MiniMalta3", (PyObject *) &PyMiniMalta3_Type);
  return MOD_RETURN(m);
}
