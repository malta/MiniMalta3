/************************************
 * PyMiniMalta3Data
 * Brief: Python module for MiniMalta3Data
 *
 * Author: Carlos.Solans@cern.ch
 *         Ignacio.Asensi@cern.ch
 ************************************/

#define PY_SSIZE_T_CLEAN //Not sure we need this
#include <Python.h>
#include <stddef.h>
#include "MiniMalta3/MiniMalta3Data.h"
#include <iostream>

#if PY_VERSION_HEX < 0x020400F0

#define Py_CLEAR(op)				\
  do {						\
    if (op) {					\
      PyObject *tmp = (PyObject *)(op);		\
      (op) = NULL;				\
      Py_DECREF(tmp);				\
    }						\
  } while (0)

#define Py_VISIT(op)				\
  do {						\
    if (op) {					\
      int vret = visit((PyObject *)(op), arg);	\
      if (vret)					\
	return vret;				\
    }						\
  } while (0)

#endif //PY_VERSION_HEX < 0x020400F0


#if PY_VERSION_HEX < 0x020500F0

typedef int Py_ssize_t;
#define PY_SSIZE_T_MAX INT_MAX
#define PY_SSIZE_T_MIN INT_MIN
typedef inquiry lenfunc;
typedef intargfunc ssizeargfunc;
typedef intobjargproc ssizeobjargproc;

#endif //PY_VERSION_HEX < 0x020500F0

#ifndef PyVarObject_HEAD_INIT
#define PyVarObject_HEAD_INIT(type, size)	\
  PyObject_HEAD_INIT(type) size,
#endif


#if PY_VERSION_HEX >= 0x03000000

#define MOD_ERROR NULL
#define MOD_RETURN(val) val
#define MOD_INIT(name) PyMODINIT_FUNC PyInit_##name(void)
#define MOD_DEF(ob,name,doc,methods)			\
  static struct PyModuleDef moduledef = {		\
    PyModuleDef_HEAD_INIT, name, doc,-1, methods	\
  };							\
  m = PyModule_Create(&moduledef);			\

#else

#define MOD_ERROR 
#define MOD_RETURN(val) 
#define MOD_INIT(name) PyMODINIT_FUNC init##name(void)
#define MOD_DEF(ob,name,doc,methods)			\
    ob = Py_InitModule3((char *) name, methods, doc);

#endif //PY_VERSION_HEX >= 0x03000000


/************************************
 *
 * Module declaration
 *
 ************************************/

typedef struct {
  PyObject_HEAD
  MiniMalta3Data *obj;
  std::vector<uint32_t> vec;
} PyMiniMalta3Data;

static int _PyMiniMalta3Data_init(PyMiniMalta3Data *self)
{
  self->obj = new MiniMalta3Data();
  return 0;
}

static void _PyMiniMalta3Data_dealloc(PyMiniMalta3Data *self)
{
  delete self->obj;
  Py_TYPE(self)->tp_free((PyObject*)self);
}


PyObject * _PyMiniMalta3Data_setDummybits(PyMiniMalta3Data *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setDummybits(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3Data_setChipid(PyMiniMalta3Data *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setChipid(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3Data_setDcolumn(PyMiniMalta3Data *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setDcolumn(pos);
  Py_INCREF(Py_None);
  return Py_None;
}


PyObject * _PyMiniMalta3Data_setPixel(PyMiniMalta3Data *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setPixel(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3Data_setGroup(PyMiniMalta3Data *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setGroup(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3Data_setBcid(PyMiniMalta3Data *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setBcid(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3Data_setFtcounter(PyMiniMalta3Data *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setFtcounter(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3Data_setPrioeow(PyMiniMalta3Data *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setPrioeow(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3Data_setWord1(PyMiniMalta3Data *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setWord1(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3Data_setWord2(PyMiniMalta3Data *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setWord2(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3Data_getDummybits(PyMiniMalta3Data *self)
{
  PyObject *py_ret;
  int ret = self->obj->getDummybits();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMalta3Data_getChipid(PyMiniMalta3Data *self)
{
  PyObject *py_ret;
  int ret = self->obj->getChipid();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMalta3Data_getDcolumn(PyMiniMalta3Data *self)
{
  PyObject *py_ret;
  int ret = self->obj->getDcolumn();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}


PyObject * _PyMiniMalta3Data_getPixel(PyMiniMalta3Data *self)
{
  PyObject *py_ret;
  int ret = self->obj->getPixel();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMalta3Data_getGroup(PyMiniMalta3Data *self)
{
  PyObject *py_ret;
  int ret = self->obj->getGroup();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMalta3Data_getBcid(PyMiniMalta3Data *self)
{
  PyObject *py_ret;
  int ret = self->obj->getBcid();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMalta3Data_getFtcounter(PyMiniMalta3Data *self)
{
  PyObject *py_ret;
  int ret = self->obj->getFtcounter();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMalta3Data_getPrioeow(PyMiniMalta3Data *self)
{
  PyObject *py_ret;
  int ret = self->obj->getPrioeow();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMalta3Data_getNhits(PyMiniMalta3Data *self)
{
  PyObject *py_ret;
  int ret = self->obj->getNhits();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMalta3Data_getWord1(PyMiniMalta3Data *self)
{
  PyObject *py_ret;
  int ret = self->obj->getWord1();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMalta3Data_getWord2(PyMiniMalta3Data *self)
{
  PyObject *py_ret;
  int ret = self->obj->getWord2();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMalta3Data_getHitRow(PyMiniMalta3Data *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
    
  PyObject *py_ret;
  int ret = self->obj->getHitRow(pos);
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMalta3Data_getHitColumn(PyMiniMalta3Data *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }

  PyObject *py_ret;
  int ret = self->obj->getHitColumn(pos);
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMalta3Data_pack(PyMiniMalta3Data *self)
{
  self->obj->pack();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3Data_unpack(PyMiniMalta3Data *self, PyObject *args)
{
  self->obj->unPack();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta3Data_toString(PyMiniMalta3Data *self)
{
    PyObject *py_ret;
    py_ret=PyUnicode_FromString(self->obj->toString().c_str());
    return py_ret;
}

PyObject * _PyMiniMalta3Data_getInfo(PyMiniMalta3Data *self)
{
    PyObject *py_ret;
    py_ret=PyUnicode_FromString(self->obj->getInfo().c_str());
    return py_ret;
}


static PyMethodDef PyMiniMalta3Data_methods[] = {
    {(char *) "setDummybits",(PyCFunction) _PyMiniMalta3Data_setDummybits , METH_VARARGS, NULL },
    {(char *) "setChipid",(PyCFunction) _PyMiniMalta3Data_setChipid , METH_VARARGS, NULL },
    {(char *) "setDcolumn",(PyCFunction) _PyMiniMalta3Data_setDcolumn,  METH_VARARGS, NULL },
    {(char *) "setPixel",(PyCFunction) _PyMiniMalta3Data_setPixel , METH_VARARGS, NULL },
    {(char *) "setGroup",(PyCFunction) _PyMiniMalta3Data_setGroup , METH_VARARGS, NULL },
    {(char *) "setBcid",(PyCFunction) _PyMiniMalta3Data_setBcid , METH_VARARGS, NULL },
    {(char *) "setFtcounter",(PyCFunction) _PyMiniMalta3Data_setFtcounter , METH_VARARGS, NULL },
    {(char *) "setPrioeow",(PyCFunction) _PyMiniMalta3Data_setPrioeow,  METH_VARARGS, NULL },
    {(char *) "setWord1",(PyCFunction) _PyMiniMalta3Data_setWord1 , METH_VARARGS, NULL },
    {(char *) "setWord2",(PyCFunction) _PyMiniMalta3Data_setWord2 , METH_VARARGS, NULL },
    {(char *) "getDummybits",(PyCFunction) _PyMiniMalta3Data_getDummybits,  METH_NOARGS, NULL },
    {(char *) "getChipid",(PyCFunction) _PyMiniMalta3Data_getChipid,  METH_NOARGS, NULL },
    {(char *) "getDcolumn",(PyCFunction) _PyMiniMalta3Data_getDcolumn,  METH_NOARGS, NULL },
    {(char *) "getPixel",(PyCFunction) _PyMiniMalta3Data_getPixel,  METH_NOARGS, NULL },
    {(char *) "getGroup",(PyCFunction) _PyMiniMalta3Data_getGroup,  METH_NOARGS, NULL },
    {(char *) "getBcid",(PyCFunction) _PyMiniMalta3Data_getBcid,  METH_NOARGS, NULL },
    {(char *) "getFtcounter",(PyCFunction) _PyMiniMalta3Data_getFtcounter,  METH_NOARGS, NULL },
    {(char *) "getPrioeow",(PyCFunction) _PyMiniMalta3Data_getPrioeow,  METH_NOARGS, NULL },
    {(char *) "getNhits",(PyCFunction) _PyMiniMalta3Data_getNhits,  METH_NOARGS, NULL },
    {(char *) "getHitColumn",(PyCFunction) _PyMiniMalta3Data_getHitColumn,  METH_VARARGS, NULL },
    {(char *) "getHitRow",(PyCFunction) _PyMiniMalta3Data_getHitRow,  METH_VARARGS, NULL },
    {(char *) "getWord1",(PyCFunction) _PyMiniMalta3Data_getWord1,  METH_NOARGS, NULL },
    {(char *) "getWord2",(PyCFunction) _PyMiniMalta3Data_getWord2,  METH_NOARGS, NULL },
    {(char *) "toString",(PyCFunction) _PyMiniMalta3Data_toString,  METH_NOARGS, NULL },
    {(char *) "getInfo",(PyCFunction) _PyMiniMalta3Data_getInfo,  METH_NOARGS, NULL },
    {(char *) "pack",(PyCFunction) _PyMiniMalta3Data_pack,  METH_NOARGS, NULL },
    {(char *) "unpack",(PyCFunction) _PyMiniMalta3Data_unpack,  METH_NOARGS, NULL },
    {NULL, NULL, 0, NULL}
};

PyTypeObject PyMiniMalta3Data_Type = {
    PyVarObject_HEAD_INIT(NULL, 0)
    (char *) "PyMiniMalta3Data.MiniMalta3Data",            /* tp_name */
    sizeof(PyMiniMalta3Data),                 /* tp_basicsize */
    0,                                   /* tp_itemsize */
    /* methods */
    (destructor) _PyMiniMalta3Data_dealloc,   /* tp_dealloc */
    (printfunc)0,                        /* tp_print */
    (getattrfunc)NULL,                   /* tp_getattr */
    (setattrfunc)NULL,                   /* tp_setattr */
    NULL,                       /* tp_compare */
    (reprfunc)NULL,                      /* tp_repr */
    (PyNumberMethods*)NULL,              /* tp_as_number */
    (PySequenceMethods*)NULL,            /* tp_as_sequence */
    (PyMappingMethods*)NULL,             /* tp_as_mapping */
    (hashfunc)NULL,                      /* tp_hash */
    (ternaryfunc)NULL,                   /* tp_call */
    (reprfunc)NULL,                      /* tp_str */
    (getattrofunc)NULL,                  /* tp_getattro */
    (setattrofunc)NULL,                  /* tp_setattro */
    (PyBufferProcs*)NULL,                /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT,                  /* tp_flags */
    NULL,                                /* Documentation string */
    (traverseproc)NULL,                  /* tp_traverse */
    (inquiry)NULL,                       /* tp_clear */
    (richcmpfunc)NULL,                   /* tp_richcompare */
    0,                                   /* tp_weaklistoffset */
    (getiterfunc)NULL,                   /* tp_iter */
    (iternextfunc)NULL,                  /* tp_iternext */
    (struct PyMethodDef*)PyMiniMalta3Data_methods, /* tp_methods */
    (struct PyMemberDef*)0,              /* tp_members */
    0,                                   /* tp_getset */
    NULL,                                /* tp_base */
    NULL,                                /* tp_dict */
    (descrgetfunc)NULL,                  /* tp_descr_get */
    (descrsetfunc)NULL,                  /* tp_descr_set */
    0,                                   /* tp_dictoffset */
    (initproc)_PyMiniMalta3Data_init,         /* tp_init */
    (allocfunc)PyType_GenericAlloc,      /* tp_alloc */
    (newfunc)PyType_GenericNew,          /* tp_new */
    (freefunc)0,                         /* tp_free */
    (inquiry)NULL,                       /* tp_is_gc */
    NULL,                                /* tp_bases */
    NULL,                                /* tp_mro */
    NULL,                                /* tp_cache */
    NULL,                                /* tp_subclasses */
    NULL,                                /* tp_weaklist */
    (destructor) NULL                    /* tp_del */
#if PY_VERSION_HEX >= 0x02060000
	,0                                   /* tp_version_tag */
#endif
};

static PyMethodDef Module_methods[] = {
  {NULL, NULL, 0, NULL}
};

MOD_INIT(PyMiniMalta3Data)
{
  PyObject *m;
  MOD_DEF(m, "PyMiniMalta3Data", NULL, Module_methods);
  if(PyType_Ready(&PyMiniMalta3Data_Type)<0){
    return MOD_ERROR;
  }
  PyModule_AddObject(m, (char *) "MiniMalta3Data", (PyObject *) &PyMiniMalta3Data_Type);
  return MOD_RETURN(m);
}
