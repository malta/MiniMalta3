/*
 *Author: leyre.flores.sanz.de.acedo@cern.ch
 *Author: Ignacio.asensi@cern.ch
 * */

#include "MiniMalta3/MiniMalta3.h"
#include <math.h>
#include <map>
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <bitset>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

MiniMalta3::MiniMalta3(){

verbose=true;
size_of_fifos=48;

defs[CLKDIV_CTRL_syncBCID_d]=0x1;
defs[CLKDIV_CTRL_syncFT_d]=0x8;
defs[CLKDIV_CTRL_prio_d]=0x04;
defs[CLKDIV_CTRL_fastcom_d]=0x08;
defs[FASTCOM_CTRL_pulseWidth_d]=0x08;
defs[AURORA_clkComp_d]=0x1;
defs[AURORA_fsp_d]=0x1;
defs[AURORA_sendSS_d]=0x0;
defs[AURORA_repeatSS_d]=0x0;
defs[AURORA_debugEn_d]=0x0;
defs[AURORA_CTRL_muxO_d]=0x0;
defs[AURORA_CTRL_muxI_d]=0x0;
defs[PERIPHERY_maskD_d]=0xFF;
defs[PERIPHERY_maskV_d]=0xFF;
defs[PERIPHERY_maskH_d]=0xFF;
defs[PERIPHERY_pulseV_d]=0x00;
defs[PERIPHERY_pulseH_d]=0x00;
defs[SYNC_enFT_d]=0xF;
defs[SYNC_enBC_d]=0xF;
defs[LAPA_enCMFB]=0x1C;
defs[LAPA_enHBRIDGE]=0x1C;
defs[LAPA_enPRE]=0x00FF;
defs[LAPA_en]=0x1;
defs[LAPA_setIBCMFB]=0x7;
defs[LAPA_setIVNH]=0x2;
defs[LAPA_setIVNL]=0xD;
defs[LAPA_setIVPH]=0xD;
defs[LAPA_setIVPL]=0x7;
defs[MONITORING_ctrlSFN]=0x9; 
defs[MONITORING_ctrlSFP]=0x5;
defs[DACS_ctrlITHR]=0x00F;
defs[DACS_ctrlIBIAS]=0x096;
defs[DACS_ctrlIRESET]=0x0FF;
defs[DACS_ctrlICASN]=0x002;
defs[DACS_ctrlIDB]=0x096;
defs[DACS_ctrlVL]=0x000;
defs[DACS_ctrlVH]=0x0FF;
defs[DACS_ctrlVRESETD]=0x082;
defs[DACS_ctrlVCLIP]=0x0FF;
defs[DACS_ctrlVCAS]=0x0A5;
defs[DACS_ctrlVREF]=0x000;
defs[FASTCOM_CTRL_bypassFC_D]=0x1;
defs[FASTCOM_CTRL_RESET_SC_d]=0x00;
defs[FASTCOM_CTRL_PULSE_SC_d]=0x00;
defs[PLL_CTRL_src_d]=0x3;
defs[DEBUG_PRIO_enableSC_d]=0x0;
defs[DEBUG_SYNC_SC_readmem_d]=0x08;
defs[PERIPHERY_delayCtrl_d]=0;//0x00000000;



/*


// ALL TO 0s

defs[CLKDIV_CTRL_syncBCID_d]=0xFFFFF;// 1;
defs[CLKDIV_CTRL_syncFT_d]=0xFFFFF;// 08;
defs[CLKDIV_CTRL_prio_d]=0xFFFFF;// 04;
defs[CLKDIV_CTRL_fastcom_d]=0xFFFFF;// 08;
defs[FASTCOM_CTRL_pulseWidth_d]=0xFFFFF;// 08;
defs[AURORA_clkComp_d]=0xFFFFF;// 1;
defs[AURORA_fsp_d]=0xFFFFF;// 1;
defs[AURORA_sendSS_d]=0xFFFFF;// 0;
defs[AURORA_repeatSS_d]=0xFFFFF;// 0;
defs[AURORA_debugEn_d]=0xFFFFF;// 0;
defs[AURORA_CTRL_muxO_d]=0xFFFFF;// 0;
defs[AURORA_CTRL_muxI_d]=0xFFFFF;// 0;
defs[PERIPHERY_maskD_d]=0xFFFFF;// FF;
defs[PERIPHERY_maskV_d]=0xFFFFF;// FF;
defs[PERIPHERY_maskH_d]=0xFFFFF;// FF;
defs[PERIPHERY_pulseV_d]=0xFFFFF;// 00;
defs[PERIPHERY_pulseH_d]=0xFFFFF;// 00;
defs[SYNC_enFT_d]=0xFFFFF;// F;
defs[SYNC_enBC_d]=0xFFFFF;// F;
defs[LAPA_enCMFB]=0xFFFFF;// 1C;
defs[LAPA_enHBRIDGE]=0xFFFFF;// 1C;
defs[LAPA_enPRE]=0xFFFFF;// 00FF;
defs[LAPA_en]=0xFFFFF;// 1;
defs[LAPA_setIBCMFB]=0xFFFFF;// 7;
defs[LAPA_setIVNH]=0xFFFFF;// 2;
defs[LAPA_setIVNL]=0xFFFFF;// D;
defs[LAPA_setIVPH]=0xFFFFF;// D;
defs[LAPA_setIVPL]=0xFFFFF;// 7;
defs[MONITORING_ctrlSFN]=0xFFFFF;// 9;
defs[MONITORING_ctrlSFP]=0xFFFFF;// 5;
defs[DACS_ctrlITHR]=0xFFFFF;// 0A0;
defs[DACS_ctrlIBIAS]=0xFFFFF;// 096;
defs[DACS_ctrlIRESET]=0xFFFFF;// 0FF;
defs[DACS_ctrlICASN]=0xFFFFF;// 002;
defs[DACS_ctrlIDB]=0xFFFFF;// 096;
defs[DACS_ctrlVL]=0xFFFFF;// 000;
defs[DACS_ctrlVH]=0xFFFFF;// 0FF;
defs[DACS_ctrlVRESETD]=0xFFFFF;// 082;
defs[DACS_ctrlVCLIP]=0xFFFFF;// 0FF;
defs[DACS_ctrlVCAS]=0xFFFFF;// 0A5;
defs[DACS_ctrlVREF]=0xFFFFF;// 005;
defs[FASTCOM_CTRL_bypassFC_D]=0xFFFFF;// 1;
defs[FASTCOM_CTRL_RESET_SC_d]=0xFF;// 00;
defs[FASTCOM_CTRL_PULSE_SC_d]=0x00;// 00;
defs[PLL_CTRL_src_d]=0xFFFFF;// 3;
defs[DEBUG_PRIO_enableSC_d]=0xFFFFF;// 0;
defs[DEBUG_SYNC_SC_readmem_d]=0xFFFFF;// 3F;
defs[PERIPHERY_delayCtrl_d]=0xFFFFFFFFFF;//0x00000000;


*/

// these words are too long for C++, so we use string in binary
defs[MATRIX_maskH_d]=1;//0xFFFFFFFFFFFF;
defs[MATRIX_pulseH_d]=1;//0x000000000000;


names[CLKDIV_CTRL_syncBCID_d]="CLKDIV_CTRL_syncBCID_d";
names[CLKDIV_CTRL_syncFT_d]="CLKDIV_CTRL_syncFT_d";
names[CLKDIV_CTRL_prio_d]="CLKDIV_CTRL_prio_d";
names[CLKDIV_CTRL_fastcom_d]="CLKDIV_CTRL_fastcom_d";
names[FASTCOM_CTRL_pulseWidth_d]="FASTCOM_CTRL_pulseWidth_d";
names[AURORA_clkComp_d]="AURORA_clkComp_d";
names[AURORA_fsp_d]="AURORA_fsp_d";
names[AURORA_sendSS_d]="AURORA_sendSS_d";
names[AURORA_repeatSS_d]="AURORA_repeatSS_d";
names[AURORA_debugEn_d]="AURORA_debugEn_d";
names[AURORA_CTRL_muxO_d]="AURORA_CTRL_muxO_d";
names[AURORA_CTRL_muxI_d]="AURORA_CTRL_muxI_d";
names[PERIPHERY_maskD_d]="PERIPHERY_maskD_d";
names[PERIPHERY_maskV_d]="PERIPHERY_maskV_d";
names[PERIPHERY_maskH_d]="PERIPHERY_maskH_d";
names[PERIPHERY_pulseV_d]="PERIPHERY_pulseV_d";
names[PERIPHERY_pulseH_d]="PERIPHERY_pulseH_d";
names[MATRIX_maskH_d]="MATRIX_maskH_d";
names[MATRIX_pulseH_d]="MATRIX_pulseH_d";
names[PERIPHERY_delayCtrl_d]="PERIPHERY_delayCtrl_d";
names[SYNC_enFT_d]="SYNC_enFT_d";
names[SYNC_enBC_d]="SYNC_enBC_d";
names[LAPA_enCMFB]="LAPA_enCMFB";
names[LAPA_enHBRIDGE]="LAPA_enHBRIDGE";
names[LAPA_enPRE]="LAPA_enPRE";
names[LAPA_en]="LAPA_en";
names[LAPA_setIBCMFB]="LAPA_setIBCMFB";
names[LAPA_setIVNH]="LAPA_setIVNH";
names[LAPA_setIVNL]="LAPA_setIVNL";
names[LAPA_setIVPH]="LAPA_setIVPH";
names[LAPA_setIVPL]="LAPA_setIVPL";
names[MONITORING_ctrlSFN]="MONITORING_ctrlSFN";
names[MONITORING_ctrlSFP]="MONITORING_ctrlSFP";
names[DACS_ctrlITHR]="DACS_ctrlITHR";
names[DACS_ctrlIBIAS]="DACS_ctrlIBIAS";
names[DACS_ctrlIRESET]="DACS_ctrlIRESET";
names[DACS_ctrlICASN]="DACS_ctrlICASN";
names[DACS_ctrlIDB]="DACS_ctrlIDB";
names[DACS_ctrlVL]="DACS_ctrlVL";
names[DACS_ctrlVH]="DACS_ctrlVH";
names[DACS_ctrlVRESETD]="DACS_ctrlVRESETD";
names[DACS_ctrlVCLIP]="DACS_ctrlVCLIP";
names[DACS_ctrlVCAS]="DACS_ctrlVCAS";
names[DACS_ctrlVREF]="DACS_ctrlVREF";
names[FASTCOM_CTRL_bypassFC_D]="FASTCOM_CTRL_bypassFC_D";
names[FASTCOM_CTRL_RESET_SC_d]="FASTCOM_CTRL_RESET_SC_d";
names[FASTCOM_CTRL_PULSE_SC_d]="FASTCOM_CTRL_PULSE_SC_d";
names[PLL_CTRL_src_d]="PLL_CTRL_src_d";
names[DEBUG_PRIO_enableSC_d]="DEBUG_PRIO_enableSC_d";
names[DEBUG_SYNC_SC_readmem_d]="DEBUG_SYNC_SC_readmem_d";

addresses["CLKDIV_CTRL_syncBCID_d"] = CLKDIV_CTRL_syncBCID_d;
addresses["CLKDIV_CTRL_syncFT_d"] = CLKDIV_CTRL_syncFT_d;
addresses["CLKDIV_CTRL_prio_d"] = CLKDIV_CTRL_prio_d;
addresses["CLKDIV_CTRL_fastcom_d"] = CLKDIV_CTRL_fastcom_d;
addresses["FASTCOM_CTRL_pulseWidth_d"] = FASTCOM_CTRL_pulseWidth_d;
addresses["AURORA_clkComp_d"] = AURORA_clkComp_d;
addresses["AURORA_fsp_d"] = AURORA_fsp_d;
addresses["AURORA_sendSS_d"] = AURORA_sendSS_d;
addresses["AURORA_repeatSS_d"] = AURORA_repeatSS_d;
addresses["AURORA_debugEn_d"] = AURORA_debugEn_d;
addresses["AURORA_CTRL_muxO_d"] = AURORA_CTRL_muxO_d;
addresses["AURORA_CTRL_muxI_d"] = AURORA_CTRL_muxI_d;
addresses["PERIPHERY_maskD_d"] = PERIPHERY_maskD_d;
addresses["PERIPHERY_maskV_d"] = PERIPHERY_maskV_d;
addresses["PERIPHERY_maskH_d"] = PERIPHERY_maskH_d;
addresses["PERIPHERY_pulseV_d"] = PERIPHERY_pulseV_d;
addresses["PERIPHERY_pulseH_d"] = PERIPHERY_pulseH_d;
addresses["MATRIX_maskH_d"] = MATRIX_maskH_d;
addresses["MATRIX_pulseH_d"] = MATRIX_pulseH_d;
addresses["PERIPHERY_delayCtrl_d"] = PERIPHERY_delayCtrl_d;
addresses["SYNC_enFT_d"] = SYNC_enFT_d;
addresses["SYNC_enBC_d"] = SYNC_enBC_d;
addresses["LAPA_enCMFB"] = LAPA_enCMFB;
addresses["LAPA_enHBRIDGE"] = LAPA_enHBRIDGE;
addresses["LAPA_enPRE"] = LAPA_enPRE;
addresses["LAPA_en"] = LAPA_en;
addresses["LAPA_setIBCMFB"] = LAPA_setIBCMFB;
addresses["LAPA_setIVNH"] = LAPA_setIVNH;
addresses["LAPA_setIVNL"] = LAPA_setIVNL;
addresses["LAPA_setIVPH"] = LAPA_setIVPH;
addresses["LAPA_setIVPL"] = LAPA_setIVPL;
addresses["MONITORING_ctrlSFN"] = MONITORING_ctrlSFN;
addresses["MONITORING_ctrlSFP"] = MONITORING_ctrlSFP;
addresses["DACS_ctrlITHR"] = DACS_ctrlITHR;
addresses["DACS_ctrlIBIAS"] = DACS_ctrlIBIAS;
addresses["DACS_ctrlIRESET"] = DACS_ctrlIRESET;
addresses["DACS_ctrlICASN"] = DACS_ctrlICASN;
addresses["DACS_ctrlIDB"] = DACS_ctrlIDB;
addresses["DACS_ctrlVL"] = DACS_ctrlVL;
addresses["DACS_ctrlVH"] = DACS_ctrlVH;
addresses["DACS_ctrlVRESETD"] = DACS_ctrlVRESETD;
addresses["DACS_ctrlVCLIP"] = DACS_ctrlVCLIP;
addresses["DACS_ctrlVCAS"] = DACS_ctrlVCAS;
addresses["DACS_ctrlVREF"] = DACS_ctrlVREF;
addresses["FASTCOM_CTRL_bypassFC_D"] = FASTCOM_CTRL_bypassFC_D;
addresses["FASTCOM_CTRL_RESET_SC_d"] = FASTCOM_CTRL_RESET_SC_d;
addresses["FASTCOM_CTRL_PULSE_SC_d"] = FASTCOM_CTRL_PULSE_SC_d;
addresses["PLL_CTRL_src_d"] = PLL_CTRL_src_d;
addresses["DEBUG_PRIO_enableSC_d"] = DEBUG_PRIO_enableSC_d;
addresses["DEBUG_SYNC_SC_readmem_d"] = DEBUG_SYNC_SC_readmem_d;

initialize_array();

/*
  m_mask_row.resize(16,0);
  m_mask_col.resize(16,0);
  m_mask_diag.resize(16,0);
  m_mask_periphery_row.resize(48,0);
 
 
  m_fifo_info=0;
  */

}


MiniMalta3::~MiniMalta3(){}

void MiniMalta3::SetVerbose(bool enable){
  verbose=enable;
  cout << "SetVerbose("<<  enable <<")"<< endl;
  //m_ipb->SetVerbose(enable);

}

void MiniMalta3::SetIPbus(ipbus::Uhal * ipb){
  m_ipb=ipb;
  CheckConnection();
  m_ipb->SetVerbose(true);
}

ipbus::Uhal * MiniMalta3::GetIPbus(){
  return m_ipb;
}

bool MiniMalta3::Connect(std::string connstr){
  m_ipb = new ipbus::Uhal(connstr);
  //m_ipb->SetVerbose(true);
  uint32_t version=0;
  cout << "Checking version on ipbus register " << endl;
  m_ipb->Read(IPBADDR_VERSION, version);

  cout << "FW VERSION: " << hex << version << endl;


  return true;//m_ipb->IsSynced();
}

void MiniMalta3::CheckConnection(){
  uint32_t version=0;
  cout << "Checking version on ipbus register " << endl;
  m_ipb->Read(IPBADDR_VERSION, version);
  //cout << "FW VERSION: " << hex << version << endl;


  if (version== 0){
    cout << endl
	 <<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl
	 << "WRONG FW VERSION OR COMMUNICATION ERROR!!!!! "
	 << version << endl
	 << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
    exit(0);
  }
}

void MiniMalta3::Reset(uint32_t value){
  if(verbose) cout << "IPBADDR SC RESET!: " << value << endl;
  m_ipb->Write(IPBADDR_SC_RESET, value );

}

void MiniMalta3::SetDefaults(bool force){
	//setting values from simple words
	if(verbose) cout << "SetDefaut(" << std::boolalpha << force << ")" <<  endl;
	for( uint32_t a = SIMPLE_WORDS_0; a <= SIMPLE_WORDS_N; a++) {
	  m_values[a]=defs[a];
	  }
	//setting values for long words
	int c=0;
	for(uint32_t  mx = 0; mx <=FIFO_WORDS_N; mx++) {
		if(verbose) cout << "Setting default....\t[" <<names[mx]<<"]\t\t["<< size_of_fifos <<"]bits" <<  endl;
		for(uint32_t i = 0; i < size_of_fifos; i++) {
			//cout << " m_fifos[" << mx <<  "]["  <<i<< "]=" << defsFIFO[mx][i] << endl;

			m_fifos[mx][i]=0;
			c=c + 1;
		}
	}


	if(force){
		WriteAll();
	}
	if(verbose) cout << "End SetDefaults" << endl;

}

uint32_t MiniMalta3::GetDefault(int pos){
  return defs[pos];
}


void MiniMalta3::Write(uint32_t pos, uint32_t value, bool force){
	if (pos < FIFO_WORDS_0 or pos > SIMPLE_WORDS_N){
		// POS DOESN'T EXIST
		cout << "Error MiniMalta3::Write() position [" << pos << "] is out of range!!" << endl;
	}else if (pos >= SIMPLE_WORDS_0 && pos <= SIMPLE_WORDS_N){
		// SIMPLE WORD with no encoding
		if(verbose) cout << " Write() on [" << names[pos] << "]\t <=\t 0x" << hex << value << dec << endl;
		m_values[pos]=value;
	}else{
		// FIFO WORD
		if(verbose) cout << "WriteFIFOword ( pos [ " << pos << " (" << names[pos] << ")]. Value ["<< value << "]" <<  endl;
		if(pos==MATRIX_maskH_d or pos==MATRIX_pulseH_d) {
			cout << "ERROR!! use dedicated methods to change Pixel pulse and masking! " <<  endl;
		}
	}
}


void MiniMalta3::WriteAll(){
  //Create a vector with all the configuration
  // 31 bit
	//if(verbose) cout << "WriteAll()" << endl;
	std::vector <uint32_t> m_values_words;
	/*
	m_values_words.push_back(0xFFFFFFFF);
	m_values_words.push_back(0xFFFFFFFF);
	m_values_words.push_back(0xFFFFFFFF);
	m_values_words.push_back(0xFFFFFFFF);
	m_values_words.push_back(0xFFFFFFFF);
	m_values_words.push_back(0xFFFFFFFF);
	m_values_words.push_back(0xFFFFFFFF);
	m_values_words.push_back(0xFFFFFFFF);
	m_values_words.push_back(0xFFFFFFFF);
	m_values_words.push_back(0xFFFFFFFF);
	m_values_words.push_back(0xFFFFFFFF);

	m_values_words.push_back(0xFFFFFFFF);
	m_values_words.push_back(0xFFFFFFFF);
	m_values_words.push_back(0xFFFFFFFF);
	m_values_words.push_back(0xFFFFFFFF);

	m_values_words.push_back(0);
	m_values_words.push_back(0);
	m_values_words.push_back(0);
	m_values_words.push_back(0);
	m_values_words.push_back(0);
	m_values_words.push_back(0);
	m_values_words.push_back(0);
	m_values_words.push_back(0);
	m_values_words.push_back(0);
	m_values_words.push_back(0);
	m_values_words.push_back(0);
	*/
	//1
	m_values_words.push_back(
		((m_values[PERIPHERY_pulseH_d]        & 0xFFFF )     << 0)|
		((m_values[LAPA_enPRE]                & 0xFFFF )     << 16));
	//2
	m_values_words.push_back(
		((m_values[PERIPHERY_maskH_d]         & 0xFFFF )     << 0)|
		((m_values[PERIPHERY_pulseV_d]        & 0xFFFF )     << 16));
	//3
	m_values_words.push_back(
		((m_values[PERIPHERY_maskD_d]         & 0xFFFF )     << 0)|
		((m_values[PERIPHERY_maskV_d]         & 0xFFFF )     << 16));
	//4
	m_values_words.push_back(
		((m_values[DACS_ctrlVCAS]      	      & 0x3FF )       << 0)|
		((m_values[DACS_ctrlVL]               & 0x3FF )       << 10)|
		((m_values[DACS_ctrlVH]               & 0x3FF )       << 20));
	//5
	m_values_words.push_back(
		((m_values[DACS_ctrlVRESETD]          & 0x3FF )       << 0)|
		((m_values[DACS_ctrlVCLIP]            & 0x3FF )       << 10)|
		((m_values[DACS_ctrlVREF]             & 0x3FF )       << 20)|
		((m_values[AURORA_clkComp_d]          & 0x1 )         << 30));
	//6
	m_values_words.push_back(
		((m_values[DACS_ctrlITHR]             & 0x3FF )       << 0)|
		((m_values[DACS_ctrlIBIAS]            & 0x3FF )       << 10)|
		((m_values[DACS_ctrlICASN]            & 0x3FF )       << 20)|
		((m_values[FASTCOM_CTRL_bypassFC_D]   & 0x1 )         << 30));
	//7
	m_values_words.push_back(
		((m_values[DACS_ctrlIDB]              & 0x3FF )       << 0)|
		((m_values[DACS_ctrlIRESET]           & 0x3FF )       << 10)|
		((m_values[FASTCOM_CTRL_PULSE_SC_d]   & 0xFF )        << 20));
	//8
	m_values_words.push_back(
		((m_values[FASTCOM_CTRL_pulseWidth_d] & 0x1F)         << 0)|
		((m_values[LAPA_enCMFB]               & 0x1F)         << 5)|
		((m_values[LAPA_enHBRIDGE]            & 0x1F)         << 10)|
		((m_values[DEBUG_SYNC_SC_readmem_d]   & 0x3F)         << 15)|
		((m_values[FASTCOM_CTRL_RESET_SC_d]   & 0xFF)         << 21)|
		((m_values[CLKDIV_CTRL_syncBCID_d]    & 0x1)          << 29));
	//9
	m_values_words.push_back(
		((m_values[LAPA_setIVNH]              & 0xF)          << 0)|
		((m_values[LAPA_setIVNL]              & 0xF)          << 4)|
		((m_values[LAPA_setIVPH]              & 0xF)          << 8)|
		((m_values[LAPA_setIVPL]              & 0xF)          << 12)|
		((m_values[CLKDIV_CTRL_syncFT_d]      & 0x1F)         << 16)|
		((m_values[CLKDIV_CTRL_prio_d]        & 0x1F)         << 21)|
		((m_values[CLKDIV_CTRL_fastcom_d]     & 0x1F )        << 26));

	m_values_words.push_back(
		((m_values[AURORA_fsp_d]              & 0x1)            << 0)|
		((m_values[AURORA_sendSS_d]           & 0x1)            << 1)|
		((m_values[AURORA_repeatSS_d]         & 0x1)            << 2)|
		((m_values[AURORA_debugEn_d]          & 0x1)            << 3)|
		((m_values[AURORA_CTRL_muxO_d]        & 0x1)            << 4)|
		((m_values[AURORA_CTRL_muxI_d]        & 0x1)            << 5)|
		((m_values[LAPA_en]                   & 0x1)            << 6)|
		((m_values[PLL_CTRL_src_d]            & 0x3)            << 7)|
		((m_values[DEBUG_PRIO_enableSC_d]     & 0x3)            << 9)|
		((m_values[MONITORING_ctrlSFN]        & 0xF)           << 11)|
		((m_values[MONITORING_ctrlSFP]        & 0xF)           << 15)|
		((m_values[SYNC_enFT_d]               & 0xF)           << 19)|
		((m_values[SYNC_enBC_d]               & 0xF)           << 23)|
		((m_values[LAPA_setIBCMFB]            & 0xF)           << 27));

	m_values_words.push_back(m_values[PERIPHERY_delayCtrl_d]);
	uint32_t word;
	int counter=0;
	// loop over the fifo words
	for(uint32_t mx = 0; mx <= FIFO_WORDS_N; mx++) {
		for(uint32_t times=0; times < 2 ; times++){
			word=0;
			for(uint32_t i = 0; i <= 31; i++) {
				word=((m_fifos[mx][i+(32*times)]  & 0x1) <<  (i+(32*times)))|word;
				if (i+(32*times)>46)break;
			}
			cout << endl;
			//if(verbose) {cout << "Writing FIFO ipbus reg\t[" << counter << "]\t "	<< "the word:\t[" << setfill('0') << setw(8) << right << hex << word << "]" << dec << endl;}
			m_values_words.push_back(word);
			m_fifos_words[counter]=word;// Add this to test dump() check--> |counter;
			//if(verbose) cout << "Fifo " << names[mx] << " chunk " << counter << ": " << word << endl;
			counter+=1;
		}
	}


	for (size_t i = 0; i < 15 ; ++i) {//m_values_words.size()
		//cout << "m_values raw: "<< hex <<  m_values_words[i] << " "<< endl;
		//m_values_words.push_back(0xFFFFFFFF);
	}
	//m_values_words.push_back(0);
	//m_values_words.push_back(0);
	//cout << "Sending Slow control word to ipbus" << endl;
	m_ipb->Write(IPBADDR_FIFO, m_values_words, true);
	//cout << "Sent to ipbus" << endl;
	/*
	for(uint32_t a = 0; a <= 15; a++) {
		cout << a << "\t" << m_values_words[a] << endl;
	}
	*/


	/*
	for(uint32_t i = 0; i <= 11; i++) {
			cout << "Writing word0:\t" << hex << "0x"<< m_values_words[i] << endl;
			//m_ipb->Write(IPBADDR_MULT_1_W, word0);
		}

	//IT IS NECESSARY TO ADD THE 4 MISSING WORDS TO FILL THE 256 FIFO BLOCK!!
	cachewordsfifos_w.push_back(0);
	cachewordsfifos_w.push_back(0);
	cachewordsfifos_w.push_back(0);
	cachewordsfifos_w.push_back(0);
	//THIS TOO: Also put them in memory to force reading the full block!!
	m_fifos_words[counter]=0;
	m_fifos_words[counter+1]=0;
	m_fifos_words[counter+2]=0;
	m_fifos_words[counter+3]=0;

	if(verbose){
	//cout << "Writing to FIFO number: " <<dec <<  counter   << " the DUMMY word:\t " << hex << 0 <<endl;
	//cout << "Writing to FIFO number: " <<dec <<  counter+1 << " the DUMMY word:\t " << hex << 0 <<endl;
	//cout << "Writing to FIFO number: " <<dec <<  counter+2 << " the DUMMY word:\t " << hex << 0 <<endl;
	//cout << "Writing to FIFO number: " <<dec <<  counter+3 << " the DUMMY word:\t " << hex << 0 <<endl;
	cout << "Writing word0:\t" << hex << "0x"<< word0 << endl;
	cout << "Writing word1:\t" << hex << "0x"<< word1 << endl;
	cout << "Writing word2:\t" << hex << "0x"<< word2 << endl;
	cout << "Writing word3:\t" << hex << "0x"<< word3 << endl;
	}
	//Finally write	 */
	/*


	m_ipb->Write(IPBADDR_MULT_2_W, word1);
	m_ipb->Write(IPBADDR_MULT_3_W, word2);
	m_ipb->Write(IPBADDR_MULT_4_W, word3);
	m_ipb->Write(IPBADDR_FIFO_W, cachewordsfifos_w, true);
	//m_ipb->Write(93,0x40);
	 */
}


uint32_t MiniMalta3::Read(int pos, bool force){
  if(force){
    uint32_t cacheWord;
    m_ipb->Read(pos, cacheWord);
    m_values_r[pos]=cacheWord;
  }
  return m_values_r[pos];
}


void MiniMalta3::ReadAll(){

  if(verbose) cout << "To be implemented()" << endl;
}

void MiniMalta3::Send(){
  WriteAll();
}

void MiniMalta3::PrintFullSCWord(){
  //First simple words
  string tabs="\t";
  for( uint32_t a = SIMPLE_WORDS_0; a <= SIMPLE_WORDS_N-3; a++) {
    cout << left << setw(22) << names[a] << right << setw(4) << hex << m_values[a] << endl;
  }
  //FIFOs
  for(uint32_t f = 0; f <= 1; f++) {
    if (names[f]<16) tabs="\t\t";
    if (names[f]<8) tabs="\t\t\t";
    cout << left << setw(25) <<names[f];
    for(uint32_t b=0; b < size_of_fifos ; b++){
      cout << m_fifos[f][b];
    }
    cout << endl;
  }
  //Remaining simple words
  for( uint32_t a = SIMPLE_WORDS_N -3; a <= SIMPLE_WORDS_N; a++) {
    cout << left << setw(22) << names[a] << right << setw(4) << hex << m_values[a] << endl;
  }
}

int MiniMalta3::DumpByWord(bool update){
  //Force ReadAll if update
  if(update){ReadAll();}

  for (uint32_t i = SIMPLE_WORDS_0 ; i <= SIMPLE_WORDS_N ; i++ ) {
    cout  << " Written " << hex << m_values[i] << "\t"<< names[i] << endl;
  }
  for(int i = 0; i <= 1; i++) {
    cout  << "Written\t0b";
    int counter=0;
    for(uint32_t  bits=0; bits < 46 ; bits++){
    	counter++;
      cout <<  m_fifos[i][bits];
      //if (counter>1)break;
    }
    cout << "\t" << names[i] <<  endl;
  }
  return 0;
}

int MiniMalta3::CheckSlowControl(bool update){
  //Force ReadAll if update
  if(update){ReadAll();}
  int errors=0;
  for (uint32_t i = SIMPLE_WORDS_0 ; i <= SIMPLE_WORDS_N ; i++ ) {
    if(m_values[i]!=m_values_r[i]){errors+=1;}
  }
  for(uint32_t i = 0; i <= 16; i++) {
    //for(uint32_t  bits=0; bits < (fifos[i+1]-fifos[i]) ; bits++){
    for(uint32_t  bits=0; bits < size_of_fifos ; bits++){
      if ( m_fifos[i][bits] != m_fifos_r[i][bits] ){ errors+=1; }
    }
  }
  return errors;
}

int MiniMalta3::DumpByIPbus(bool update){
  //Force ReadAll if update
  if(update){ReadAll();}
  /*
  // compare simple registers
  int error_c = 0;
  for( uint32_t a = 0; a <4; a++) {
    if (m_values_words[a]!=m_values_words_r[a]){
      if(verbose) cout << "Word num\t[" << dec << a << "]\t " <<  names[a] << " = "
		      << "\t 0x" << hex << m_values_words_r[a] << dec
		       << "\t Expected: " << hex << m_values_words[a] << dec
		       <<endl;
      error_c+=1;
    }
  }
  // compare fifo words
  for( uint32_t a = 0; a <136; a++) {
    if (m_fifos_words[a]!=m_fifos_words_r[a]){
      //MISMATCH
      if(verbose) cout << "Reg num\t[" << dec << a << "]\t ="
		       << "\t 0x" << hex << m_fifos_words_r[a] << dec
		       << "\t Expected: " << hex << m_fifos_words[a] << dec
		       << endl;
      error_c+=1;
    }
  }
  cout << "Dump(" << std::boolalpha << update << ") Number ipbus registers with errors: " << dec <<  error_c << endl;
  return error_c;
  */
  return 0;
}


void MiniMalta3::SetState(int val){
	//cout << "SetState " << val << endl;
  m_ipb->Write(IPBADDR_CONTROL,val);
}

uint32_t MiniMalta3::Word(std::string word){
	if (addresses.count(word)){
		return addresses[word];
	}else{
		cout << "ERROR!! [" << word << "] does not exist!!" << endl;
		return 0;
	}

}

void MiniMalta3::thermomiter_encoder(int pos, uint32_t  value){
  div_t vv;
  for(int i=0; i<128; i++){
    m_fifos[pos][i]=0;
  }
  vv=std::div(value,2);
  for (int i= 63-vv.quot; i<63; i++){ m_fifos[pos][i]=1;}
  for (int i=63; i<63+vv.quot+vv.rem; i++){ m_fifos[pos][i]=1;}
  if(verbose){
    cout << "Thermometer encoding " << value << " to  => 0b" ;
    for (int i=0; i<128; i++){
      cout <<  m_fifos[pos][i] ;
    }
    cout << endl;
  }
}

void MiniMalta3::hot_encoder(int pos, uint32_t  value){
  if(value>127) value=127;
  value=127-value;
  for(int i = 0; i < size_of_fifos; i++) {
    m_fifos[pos][i]=0;
  }
  m_fifos[pos][value]=1;
}

void MiniMalta3::initialize_array(){
	//cout << "initialize_array" <<  endl;
	for(uint32_t  f = FIFO_WORDS_0; f <=2; f++) {
		//cout << "f" << f <<  endl;
		for(uint32_t i = 0; i < 48; i++) {
			//cout << "i" << i <<  endl;
			defsFIFO[f][i]=0;
		}
	}

}

void MiniMalta3::SetPixelMaskRow(uint32_t row, bool enable){
	if(row>47){
		cout << "SetPixelMaskRow Row out of range"<< endl;
	}else{
		if(verbose) cout << "SetPixelMaskRow to " << row << endl;
		m_fifos[MATRIX_maskH_d][row]=enable;
	}
}

void MiniMalta3::SetPixelPulseRow(uint32_t row, bool enable){
	if(row>47){
		cout << "SetPixelPulseRow Row out of range"<< endl;
	}else{
		if(verbose) cout << "SetPixelPulseRow to " << row << endl;
		m_fifos[MATRIX_pulseH_d][row]=enable;
	}
}

uint32_t MiniMalta3::invertBinaryOrder(uint32_t num){
	uint32_t result =0;
	while (num > 0){
		result <<=1;
		//result |= (num & 1);
		if (num &1 ==1){
			result ^= 1;		
		}		
		num >>= 1;
	}
	return result;
}

uint32_t MiniMalta3::reverse_byte(uint32_t x)
{
	uint32_t  table[] = {
        0x00, 0x80, 0x40, 0xc0, 0x20, 0xa0, 0x60, 0xe0,
        0x10, 0x90, 0x50, 0xd0, 0x30, 0xb0, 0x70, 0xf0,
        0x08, 0x88, 0x48, 0xc8, 0x28, 0xa8, 0x68, 0xe8,
        0x18, 0x98, 0x58, 0xd8, 0x38, 0xb8, 0x78, 0xf8,
        0x04, 0x84, 0x44, 0xc4, 0x24, 0xa4, 0x64, 0xe4,
        0x14, 0x94, 0x54, 0xd4, 0x34, 0xb4, 0x74, 0xf4,
        0x0c, 0x8c, 0x4c, 0xcc, 0x2c, 0xac, 0x6c, 0xec,
        0x1c, 0x9c, 0x5c, 0xdc, 0x3c, 0xbc, 0x7c, 0xfc,
        0x02, 0x82, 0x42, 0xc2, 0x22, 0xa2, 0x62, 0xe2,
        0x12, 0x92, 0x52, 0xd2, 0x32, 0xb2, 0x72, 0xf2,
        0x0a, 0x8a, 0x4a, 0xca, 0x2a, 0xaa, 0x6a, 0xea,
        0x1a, 0x9a, 0x5a, 0xda, 0x3a, 0xba, 0x7a, 0xfa,
        0x06, 0x86, 0x46, 0xc6, 0x26, 0xa6, 0x66, 0xe6,
        0x16, 0x96, 0x56, 0xd6, 0x36, 0xb6, 0x76, 0xf6,
        0x0e, 0x8e, 0x4e, 0xce, 0x2e, 0xae, 0x6e, 0xee,
        0x1e, 0x9e, 0x5e, 0xde, 0x3e, 0xbe, 0x7e, 0xfe,
        0x01, 0x81, 0x41, 0xc1, 0x21, 0xa1, 0x61, 0xe1,
        0x11, 0x91, 0x51, 0xd1, 0x31, 0xb1, 0x71, 0xf1,
        0x09, 0x89, 0x49, 0xc9, 0x29, 0xa9, 0x69, 0xe9,
        0x19, 0x99, 0x59, 0xd9, 0x39, 0xb9, 0x79, 0xf9,
        0x05, 0x85, 0x45, 0xc5, 0x25, 0xa5, 0x65, 0xe5,
        0x15, 0x95, 0x55, 0xd5, 0x35, 0xb5, 0x75, 0xf5,
        0x0d, 0x8d, 0x4d, 0xcd, 0x2d, 0xad, 0x6d, 0xed,
        0x1d, 0x9d, 0x5d, 0xdd, 0x3d, 0xbd, 0x7d, 0xfd,
        0x03, 0x83, 0x43, 0xc3, 0x23, 0xa3, 0x63, 0xe3,
        0x13, 0x93, 0x53, 0xd3, 0x33, 0xb3, 0x73, 0xf3,
        0x0b, 0x8b, 0x4b, 0xcb, 0x2b, 0xab, 0x6b, 0xeb,
        0x1b, 0x9b, 0x5b, 0xdb, 0x3b, 0xbb, 0x7b, 0xfb,
        0x07, 0x87, 0x47, 0xc7, 0x27, 0xa7, 0x67, 0xe7,
        0x17, 0x97, 0x57, 0xd7, 0x37, 0xb7, 0x77, 0xf7,
        0x0f, 0x8f, 0x4f, 0xcf, 0x2f, 0xaf, 0x6f, 0xef,
        0x1f, 0x9f, 0x5f, 0xdf, 0x3f, 0xbf, 0x7f, 0xff,
    };
    return table[x];
}

void MiniMalta3::SendPulse(string request){
	cout << "ERROR: SendPulse. This function is not available in any FW anymore" << endl;
	/*
	if (request.compare("SCV_PULSE") == 0){
		cout << "Sending pulse to IPBADDR_SCV_PULSE" << endl;
		m_ipb->Write(IPBADDR_SCV_PULSE, 1 );
		m_ipb->Write(IPBADDR_SCV_PULSE, 0 );
	}else if (request.compare("VREF_PULSE") == 0){
		cout << "Sending pulse to IPBADDR_VREF_PULSE" << endl;
		m_ipb->Write(IPBADDR_VREF_PULSE, 1 );
		m_ipb->Write(IPBADDR_VREF_PULSE, 0 );
	}else{
		cout << "ERROR Pulsing" << endl;
	}
	*/
}


void MiniMalta3::SetDAC(uint32_t pos, uint32_t value, bool enable, bool overide){
	//cout << "Setting DAC ["<<  names[pos] << "] to [" << value << "]" <<  endl;
	/*
	m_values[pos]= (
			((value & 0xFF) << 0)|
			((enable   & 0x1 )     << 8)|
			((overide     & 0x1 )  << 9));


	*/
	uint32_t inv_value;
	inv_value=reverse_byte(value);
	m_values[pos]= (
		((inv_value & 0xFF) << 0)|
		((enable   & 0x1 )     << 8)|
		((overide   & 0x1)     << 9));

	/*
	m_values[pos]= (
			((overide   & 0x1) << 0)|
			((enable   & 0x1 )     << 1)|
			((inv_value & 0xFF)     << 2));
	*/

	if(verbose) cout << "RAW ["<<  names[pos] << "] " << bitset<10>(m_values[pos]) <<  endl;

}

uint32_t MiniMalta3::GetDebugFIFOSize(){
	uint32_t tmp;
	m_ipb->Read(IPBADDR_SLOWDATA_MON, tmp);
	cout << "Raw ipbus register from FIFO counters: " << tmp << endl;
	return (tmp or  0xFF ) >> 9;
}




vector<uint32_t> MiniMalta3::GetDebugFIFOData(){
	vector<uint32_t> cachewordsfifos_r;
	m_ipb->Read(IPBADDR_SLOWDATA, cachewordsfifos_r, 255, true);
	 for(uint32_t i=0; i < cachewordsfifos_r.size(); i++){
    	cout << "FIFO word \t[" << i <<  "]\t raw data: " << cachewordsfifos_r.at(i) << endl;
  	}
	return cachewordsfifos_r;
}
void MiniMalta3::SendFastComSerial(uint32_t cmd ,uint32_t data){
	uint32_t word=0;
	word=
			((cmd        & 0xFF )     << 0)|
			((data       & 0xFF )     << 16);

	m_ipb->Write(IPBADDR_FASTCOM_SERIAL,word);
	m_ipb->Write(IPBADDR_FASTCOM_CONTROL,1);
	m_ipb->Write(IPBADDR_FASTCOM_CONTROL,0);
}

